package parfam {
  import parfam._
  import java.io.File

  object Parfam {
    val cmsearch_hadoop_usage = """
Usage: (for running on a cluster):
  hadoop jar omics.jar cmsearch.hadoop inputfile outputdir

    NOTE: You may need to apply hadoop.patch (in omics.jar) to Hadoop
          for Hadoop's invocation of Java to not run out of heap space.

    NOTE: Hadoop is assumed to be Cloudera hadoop-0.20.2-cdh3u2, but
          should also work with Apache Hadoop 0.20.2.  They assume Java 1.6.
          You may try running with other Hadoop versions.

DESCRIPTION:
cmsearch output for genome will be placed in a cmsearch subdirectory of
genome.fna's parent directory.  Hadoop output will be placed in outputdir.

INPUT FILE FORMAT:
/path/to/genome.fna /path/to/model
/path/to/genome.fna /path/to/model
...

INPUT FILE EXAMPLE:
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0002
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0003
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0005
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0006
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0007
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0008
/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2517093047/2517093047.fna  /global/homes/s/schaumbe/models/non-tRNA-rRNA/0009
"""
    def cmsearchHadoop(args: Array[String]) {
      if (args.length < 2) {
        println("Too few args")
        println(cmsearch_hadoop_usage)
        sys.exit(1)
      }
      CmsearchHadoop.main(args) // calls System.exit
    }

    val cmsearch_local_usage = """
Stand-alone usage (for running on a single workstation):
  java -jar omics.jar cmsearch.local --genome genome.fna \
                                     [gff-options] \
                                     [model-options] \
                                     [parallism-options]

    NOTE: If running with more than 1 worker_threads, Java 1.7 works
          best, due to ForkJoinTasks' work-stealing.
          Java 1.6 works fine, albeit without work-stealing, which means
          if a worker has no more work to do, it won't take work from
          a worker that has work yet to do.

DESCRIPTION:
cmsearch output for genome will be placed in a cmsearch subdirectory of
genome.fna's parent directory.  The path to genome.fna must be given.


[gff-options]:
--gff filename.gff
    If "--gff filename.gff" is specified, a GFF version 3 file named
    filename.gff will be written, derived from all cmsearch output.  As an
    extra sanity check, the last line in filename.gff should be "# [ok]",
    similar to the last lines in the cmsearch output files.  If this
    "# [ok]" line is not present, something is wrong, e.g. some cmsearch
    processes failed or the file became truncated.

--resolve_overlaps
    In filename.gff (given by --gff filename.gff), discard features having
    the same seqid but lower bitscores, overlapping on the same strand.
    The GFF then has the "best nonoverlapping hits" for each feature seqid.
    If --resolve_overlaps is not specified, all the cmsearch hits are
    simply written to the GFF, without any discarding. 


[model-options]:
Models may be specified individually, or a directory may be given, assuming
that directory has model files in it.

--model infernal_model
    The path to the model file to use, typically from Rfam.
    --worker_threads is ignored in this case, as there's only one work item.

--model_dir infernal_model_directory
    The directory containing model files.

[--chunk_div process_models_in_X_subsets]
[--chunk_id process_the_Xth_subset]
    For processing only a subset of the models in the model_dir, specify
    chunk_div and chunk_id.  If chunk_div is 13, chunkid is 3 (the first
    chunkid is 0, last is chunk_div - 1), and the models in model_dir are
    "a", "b", "c", ..., and "z", then only models "e" and "f" are processed
    here.
    In this way, the 26 models could be processed in subsets of 2, potentially
    by 13 concurrent omics.jar instances.


[parallelism-options]:
--cmsearch_threads X
    Invoke 'cmsearch --cpu X ...' for each model.
    Default is X=1

--worker_threads Y
    Use Y concurrent 'cmsearch --cpu X ...' processes, one model per cmsearch,
    to finish all specified models.
    Default is Y=1


EXAMPLE:
java -jar omics.jar cmsearch.local --genome genome.fna \
  --gff genome.cmsearch.gff --model_dir /path/dir/containing/rfam/models
"""
    def cmsearchLocalUsage(help: String): Unit = {
      if (help != "") {
        println(help)
      }
      println(cmsearch_local_usage)
      sys.exit(1)
    }
    def cmsearchLocal(args: Array[String]) {
      var cmsearchthreads = 1
      var workerthreads = 1
      var onemodel = false
      var manymodels = false
      var modelfilename = ""
      var modeldirname = ""
      var genomefilename = ""
      var chunkdiv = 1
      var chunkid = 0
      var model: File = null
      var modeldir: File = null
      var gfffilename = ""
      var gfffile: File = null
      var resolveoverlaps = false

      // two args per var, e.g. "--foo bar".
      // need genome and model at least.
      if (args.length < 4) cmsearchLocalUsage("Too few args")

      var badarg = false
      def nextOption(list: List[String]) : Unit = {
        list match {
          case Nil => {}
          case "--chunk_div" :: value :: tail => {
            chunkdiv = value.toInt
            nextOption(tail)
          }
          case "--chunk_id" :: value :: tail => {
            chunkid = value.toInt
            nextOption(tail)
          }
          case "--cmsearch_threads" :: value :: tail => {
            cmsearchthreads = value.toInt
            manymodels = true
            nextOption(tail)
          }
          case "--genome" :: value :: tail => {
            genomefilename = value.toString
            nextOption(tail)
          }
          case "--gff" :: value :: tail => {
            gfffilename = value.toString
            nextOption(tail)
          }
          case "--model" :: value :: tail => {
            modelfilename = value.toString
            onemodel = true
            nextOption(tail)
          }
          case "--model_dir" :: value :: tail => {
            modeldirname = value.toString
            manymodels = true
            nextOption(tail)
          }
          case "--resolve_overlaps" :: tail => {
            resolveoverlaps = true
            nextOption(tail)
          }
          case "--worker_threads" :: value :: tail => {
            workerthreads = value.toInt
            nextOption(tail)
          }
          case option :: tail => {
            println("Unknown option: " + option) 
            badarg = true
            nextOption(tail)
          }
        }
      }
      nextOption(args.toList)

      if (badarg) {
        cmsearchLocalUsage("")
      }
      if (cmsearchthreads < -1) {
        cmsearchLocalUsage("--cmsearch_threads invalid value: " +
                           cmsearchthreads +
                           ".  Specify -1 or greater.  See cmsearch -h.")
      }
      if (workerthreads < 1) {
        cmsearchLocalUsage("--worker_threads invalid value: " +
                           workerthreads +
                           ".  Specify 1 or greater.")
      }

      if (genomefilename == "") {
        cmsearchLocalUsage("--genomefile not defined, don't know what genome FASTA is")
      }
      val genomefile = new File(genomefilename)
      if (!genomefile.exists) {
        cmsearchLocalUsage("Genome file doesn't exist: " + genomefilename)
      }

      if (gfffilename != "") {
        gfffile = new File(gfffilename)
      }

      if (onemodel) {
        if (manymodels) {
          cmsearchLocalUsage("Specify --model (one file) or --model_dir (holds many files), not both")
        }

        if (modelfilename == "") {
          cmsearchLocalUsage("--model_dir not defined, don't know where model files are")
        }
        model = new File(modelfilename)
        if (!model.exists) {
          cmsearchLocalUsage("Model doesn't exist: " + modelfilename)
        }
      } else {
        if (!manymodels) {
          cmsearchLocalUsage("Specify --model (one file) or --model_dir (holds many files)")
        }

        if (modeldirname == "") {
          cmsearchLocalUsage("--model_dir not defined, don't know where model files are")
        }
        modeldir = new File(modeldirname)
        if (!modeldir.exists) {
          cmsearchLocalUsage("Model directory doesn't exist: " + modeldirname)
        }

        if (chunkdiv < 1) {
          cmsearchLocalUsage("Invalid chunkdiv: " + chunkdiv)
        }
        if (chunkid < 0 || chunkid >= chunkdiv) {
          cmsearchLocalUsage("Invalid chunkid: " + chunkid)
        }
      }

      if (onemodel) {
        val result = (new parfam.Infernal).runModel(model, genomefile, cmsearchthreads)
        println("Done, exitcode: %d".format(result))
      } else if (manymodels) {
        val result = (new parfam.Infernal).runModels(modeldir, genomefile, chunkdiv, chunkid,
                                                     workerthreads, cmsearchthreads,
                                                     if (gfffile != null) {Some(gfffile)} else {None},
                                                     resolveoverlaps)
        println("Done, exitcodes: %s".format(result))
      }
    }

    val command_usage_brief = """
Use command "help" for more detail.

Usage: java  -jar omics.jar command args
"""
    val command_usage = """
Known commands are:
  cmgbk           : compare cmsearch and GenBank Infernal hits
  cmgff           : compare hits among two GFF (version 3) files
  cmsearch.hadoop : find Infernal hits in a genome, on your supercomputer
  cmsearch.local  : find Infernal hits in a genome, on your workstation
  fastats         : FASTA stats, e.g. # bases, GC content
  pal             : stem-loop motif detection by palindromic sequence search (code stub)
  randgrp         : random subsample without replacement, of groups
  randgrpr        : random subsample with    replacement, of groups (fast)
  randsub         : random subsample without replacement, of file lines

For more help regarding a particular command, use:
  java  -jar omics.jar command help

Usage:
  java  -jar omics.jar command args

This is a late-July 2013 build of omics.jar
"""
    def main(args: Array[String]) {
      if (args.length < 1) {
        println("Too few args, must specify command.")
        println(command_usage_brief)
        sys.exit(1)
      }
      args.head match {
        case "cmgbk" => {
          CmsearchCmpGenBank.runArgs(args.tail)
        }
        case "cmgff" => {
          CmsearchCmpGff.runArgs(args.tail)
        }
        case "cmsearch.local" => {
          cmsearchLocal(args.tail)
        }
        case "cmsearch.hadoop" => {
          cmsearchHadoop(args.tail)
        }
        case "fastats" => {
          Fastats.runArgs(args.tail)
        }
        case "pal" => {
          PalindromeCPU.runArgs(args.tail)
        }
        case "randgrp" => {
          RandomGroup.runArgs(args.tail)
        }
        case "randgrpr" => {
          RandomRepGroup.runArgs(args.tail)
        }
        case "randsub" => {
          RandomSubsample.runArgs(args.tail)
        }
        case "help" => {
          println(command_usage)
          sys.exit(1)
        }
        case oops => {
          println("Unknown command: %s" format oops)
          println(command_usage_brief)
          sys.exit(1)
        }
      }
    }
  }
}
