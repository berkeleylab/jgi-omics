package parfam;
import parfam.Infernal;
import parfam.OneLineInputFormat;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;
import java.lang.Iterable;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class CmsearchHadoop extends Configured implements Tool {
  public static class CmsearchHeartbeat extends Thread {
    private volatile int beats;
    private volatile Thread thread;
    private final Progressable pro;
    private static final int INTERVAL = 1000;
    CmsearchHeartbeat(Progressable pro, int beats) {
      this.beats = beats;
      this.pro = pro;
      this.thread = null;
    }
    public void run() {
      this.thread = Thread.currentThread();
      while (beats > 0) {
        pro.progress(); // says this Hadoop Task's OK
        beats--;
        try {
          this.thread.sleep(INTERVAL);
        } catch (InterruptedException e) {
        }
      }
    }
    public void die() {
      this.beats = 0;
      if (this.thread != null) {
        this.thread.interrupt();
      }
    }
    public void end() {
      if (this.thread != null) {
        try {
          this.thread.join(INTERVAL);
        } catch (InterruptedException e) {
        }
      }
    }
  }
  public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable zero = new IntWritable(0);
    private final Text word = new Text();
    private final Infernal infernal = new Infernal();

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
      String line = value.toString();
      StringTokenizer tokenizer = new StringTokenizer(line);

      File genome = new File(tokenizer.nextToken());
      File model = new File(tokenizer.nextToken());

      // 150 cpu-minutes in millis, which is a long cmsearch
      // high cpu long may make heartbeat beat slower
      // slow heartbeat is ok provided cmsearch eventually terminates
      CmsearchHeartbeat hb = new CmsearchHeartbeat(context, 9000000);
      hb.start();

      int ret = infernal.runModel(model, genome, 1);

      hb.die();
      if (ret == 0) {
        context.write(word, zero);
      } else {
        context.write(word, new IntWritable(ret));
      }
      hb.end();
    }
  }

  public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
      int exitcodes = 0;
      for (IntWritable val : values) {
        exitcodes |= val.get();
      }
      context.write(key, new IntWritable(exitcodes)); // all exit codes should OR to 0 if all good
    }
  }

  @Override
  public int run(String[] args) throws Exception {
    Configuration conf = getConf();

    Job job = new Job(conf, "cmsearch.hadoop");

    job.setJarByClass(WordCount.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);

    job.setMapperClass(Map.class);
    job.setReducerClass(Reduce.class);

    //job.setInputFormatClass(TextInputFormat.class);
    job.setInputFormatClass(OneLineInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    return job.waitForCompletion(true) ? (0) : (1);
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    System.exit( ToolRunner.run(conf, new CmsearchHadoop(), args) );
  }
}
