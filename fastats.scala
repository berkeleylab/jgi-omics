package parfam {
  import java.io.File
  import scala.io.Source // for reading files
  import scala.collection.immutable.TreeMap

  object Fastats {
    val fasta_stats_usage = """
Usage:
java -jar omics.jar fastats --file my.fasta

Statistics for FASTA files of nucleic acids or amino acids.

GC content calculated as (G+C)/allbases, where allbases may include N etc.
"""
    def usage(msg: String): Unit = {
      if (msg != "") {
        println(msg)
      }
      println(fasta_stats_usage)
      sys.exit(1)
    }
    def runArgs(args: Array[String]): Unit = {
      if (args.length < 1) {
        usage("Too few args")
      }
      var badarg = false
      var infilename: String = null
      def nextOption(list: List[String]) : Unit = {
        list match {
          case Nil => {}
          case "--file" :: value :: tail => {
            infilename = value
            nextOption(tail)
          }
          case option :: tail => {
            println("Unknown option: " + option) 
            badarg = true
            nextOption(tail)
          }
        }
      }
      nextOption(args.toList)
      
      if (badarg) {
        usage("")
      }
      if (infilename == null) {
        usage("Please specify a --file")
      }
      val infile = new File(infilename)
      if (!infile.exists) {
        usage("Input --file doesn't exist")
      }
      (new Fastats(infile)).run
    }
  }
  class Fastats(_fastafile: File) {
    val fastafile = _fastafile
    def run: Unit = {
      var readtotal = 0
      var basetotal = 0
      var basecounts = new TreeMap[Char, Int]

      val fastalines = Source.fromFile(fastafile).getLines
      while (fastalines.hasNext) {
        val line = fastalines.next
        if (line.startsWith(">")) {
          readtotal += 1
        } else if (!line.startsWith("#")) {
          //not a comment, so got bases, probably.  may be empty line.
          val l = line.trim
          for (i <- 0 to l.length - 1) {
            val c = l.charAt(i)
            val count = basecounts.get(c).getOrElse(0)
            basecounts += (c -> (count + 1))
            basetotal += 1
          }
        }
      }

      println("reads: %12d" format readtotal)
      println("bases: %12d" format basetotal)
      for ((key, value) <- basecounts) {
        println("count %c: %10d" format (key, value))
      }

      // biopython and others define gc content to be the number of
      // g and c bases divided by all bases (which includes n etc)
      // http://biopython.org/DIST/docs/api/Bio.SeqUtils-module.html#GC
      val gtotal = basecounts.get('g').getOrElse(0) + basecounts.get('G').getOrElse(0)
      val ctotal = basecounts.get('c').getOrElse(0) + basecounts.get('C').getOrElse(0)
      println("gc content: %2.4f" format (
        100.0 * (gtotal + ctotal).toFloat / basetotal.toFloat))
    }
  }
}
