package parfam

import java.io.{BufferedReader,File,FileReader,PrintWriter}
import scala.io.Source // for reading files

import org.biojava.bio._
import org.biojava.bio.seq._
import org.biojava.bio.seq.io._
import org.biojavax._
import org.biojavax.bio._
import org.biojavax.bio.seq._ // richsequence
import scala.collection.JavaConversions._ // use java.util.Iterator from features() within a scala for(...)

import scala.collection.immutable.TreeMap
//import scala.collection.immutable.TreeSet

object CmsearchCmpGenBank {
  val cmsearch_cmp_gb_usage = """
Usage:
java -jar omics.jar cmgbk --gbk my.gbk [--tbl my.gbk.fna.rfam.out]

Compare cmsearch and GenBank files for features, by
  identity     : feature in both files with start and stop within 9 nucleotides
  subfeature   : feature in tbl contained within feature in gbk
  superfeature : feature in tbl contains feature in gbk
  5' overlap   : feature in tbl shifted 5' but still overlaps feature in gbk
  3' overlap   : feature in tbl shifted 3' but still overlaps feature in gbk
  5' shift     : feature in tbl shifted 5' and doesn't overlap feature in gbk
  3' shift     : feature in tbl shifted 3' and doesn't overlap feature in gbk
  miss         : feature not present in tbl but present in gbk
  novel        : feature present in tbl but not present in gbk

A feature is typically an ncRNA, given cmsearch was used to find Rfams.

If --tbl not specified, cmsearch will be run against the ORIGIN gbk sequence.
TBD: The cmsearch results will include cmsearch tblout files, processed, then used for comparison.
"""
  def usage(msg: String): Unit = {
    if (msg != "") {
      println(msg)
    }
    println(cmsearch_cmp_gb_usage)
    sys.exit(1)
  }
  def runArgs(args: Array[String]): Unit = {
    if (args.length < 2) {
      usage("Too few args")
    }
    var badarg = false
    var tblfilename: String = null
    var gbkfilename: String = null
    def nextOption(list: List[String]) : Unit = {
      list match {
        case Nil => {}
        case "--tbl" :: value :: tail => {
          tblfilename = value
          nextOption(tail)
        }
        case "--gbk" :: value :: tail => {
          gbkfilename = value
          nextOption(tail)
        }
        case option :: tail => {
          println("Unknown option: " + option) 
          badarg = true
          nextOption(tail)
        }
      }
    }
    nextOption(args.toList)
    
    if (badarg) {
      usage("")
    }
    var tblfile: File = null
    if (tblfilename != null) {
      tblfile = new File(tblfilename)
      if (!tblfile.exists) {
        usage("Given --tbl doesn't exist")
      }
    }
    if (gbkfilename == null) {
      usage("Please specify a --gbk")
    }
    val gbkfile = new File(gbkfilename)
    if (!gbkfile.exists) {
      usage("Given --gbk doesn't exist")
    }
    (new CmsearchCmpGenBank(tblfile, gbkfile)).run
  }
}

//example of a rfam to match up
//
//schaumbe@gpint06:~/proj/omics/gbk/analyze/10005$ grep RF00010 /scratch/schaumbe/t/cmsearch/*/cmsearch.tblout
///scratch/schaumbe/t/cmsearch/0009/cmsearch.tblout:A040L21DRAFT_contig_9_0.10 -         RNaseP_bact_a        RF00010    cm       13      319    11551    11944      +    no    1 0.47   0.6   79.3   1.8e-26 !   -
//schaumbe@gpint06:~/proj/omics/gbk/analyze/10005$ grep -3 RF00010 *gbk
//                     /gene_calling_method="Prodigal V2.50: November, 2010"
//     misc_RNA        11547..11974
//                     /locus_tag="A040L21DRAFT_00179"
//                     /RNA_Class_ID="RF00010"
//                     /product="Bacterial RNase P class A"
//                     /gene_calling_method="INFERNAL 1.0.2 (October 2009)"
//                     /Model="RNaseP_bact_a"
object RelPos extends Enumeration {
  val UP, SAME, DOWN = Value // upstream, same, downstream relative positions
}
class Feat(_id: String, _start: Int, _end: Int) {
  val id = _id
  val start = _start
  val end = _end
  var verdict: String = null

  def getId: String = return id
  def getStart: Int = return start
  def getEnd: Int = return end
  def getVerdict: String = return verdict
  def setVerdict(v: String): Unit = {verdict = v}
  def maybeSetVerdict(v: String): Unit = {
    if (verdict == null) setVerdict(v)
  }

  def toVerdict(defaultVerdict: String): String = {
    var v = verdict
    if (v == null) {
      v = defaultVerdict
    }
    return ("feat %s %d-%d: %s" format (id, start, end, v))
  }

  def eq(that: Feat): Boolean = return (id == that.getId)

  def doIdentity(that: Feat): Unit = {
    val ts = this.getStart //tblfeat
    val te = this.getEnd
    val gs = that.getStart //gbkfeat
    val ge = that.getEnd

    if (math.abs(ts - gs) <= 9 &&
        math.abs(te - ge) <= 9) {
      this.maybeSetVerdict("identity")
      that.maybeSetVerdict("identity")
      return
    }
  }
  def doSubSuper(that: Feat): Unit = {
    val ts = this.getStart //tblfeat
    val te = this.getEnd
    val gs = that.getStart //gbkfeat
    val ge = that.getEnd

    var relstart = RelPos.SAME
    if (ts < gs) {
      relstart = RelPos.UP
      //println("relstart UP")
    } else if (ts > gs) {
      relstart = RelPos.DOWN
      //println("restart DOWN")
    }

    var relend = RelPos.SAME
    if (te < ge) {
      relend = RelPos.UP
      //println("relend UP")
    } else if (te > ge) {
      relend = RelPos.DOWN
      //println("relend DOWN")
    }

    //println("relstart: %s, relend %s" format (relstart, relend))

    if ((relstart == RelPos.DOWN && relend == RelPos.SAME) ||
        (relstart == RelPos.SAME && relend == RelPos.UP  ) ||
        (relstart == RelPos.DOWN && relend == RelPos.UP  )) {
      //println("got sub")
      this.maybeSetVerdict("subfeature")
      that.maybeSetVerdict("superfeature")
      return
    }
    if ((relstart == RelPos.UP   && relend == RelPos.SAME) ||
        (relstart == RelPos.SAME && relend == RelPos.DOWN) ||
        (relstart == RelPos.UP   && relend == RelPos.DOWN)) {
      //println("got sup")
      this.maybeSetVerdict("superfeature")
      that.maybeSetVerdict("subfeature")
      return
    }
  }
  def doOverlap(that: Feat): Unit = {
    val ts = this.getStart //tblfeat
    val te = this.getEnd
    val gs = that.getStart //gbkfeat
    val ge = that.getEnd

    var relstart = RelPos.SAME
    if (ts < gs) {
      relstart = RelPos.UP
      //println("relstart UP")
    } else if (ts > gs) {
      relstart = RelPos.DOWN
      //println("restart DOWN")
    }

    var relend = RelPos.SAME
    if (te < ge) {
      relend = RelPos.UP
      //println("relend UP")
    } else if (te > ge) {
      relend = RelPos.DOWN
      //println("relend DOWN")
    }

    if ((relstart == RelPos.UP   && relend == RelPos.UP   && te >= gs) ||
        (relstart == RelPos.DOWN && relend == RelPos.DOWN && ts <= ge)) {
      this.maybeSetVerdict("overlap")
      that.maybeSetVerdict("overlap")
      return
    }
  }
  def doShift(that: Feat): Unit = {
    val ts = this.getStart //tblfeat
    val te = this.getEnd
    val gs = that.getStart //gbkfeat
    val ge = that.getEnd

    var relstart = RelPos.SAME
    if (ts < gs) {
      relstart = RelPos.UP
      //println("relstart UP")
    } else if (ts > gs) {
      relstart = RelPos.DOWN
      //println("restart DOWN")
    }

    var relend = RelPos.SAME
    if (te < ge) {
      relend = RelPos.UP
      //println("relend UP")
    } else if (te > ge) {
      relend = RelPos.DOWN
      //println("relend DOWN")
    }

    if ((relstart == RelPos.UP   && relend == RelPos.UP   && te < gs) ||
        (relstart == RelPos.DOWN && relend == RelPos.DOWN && ts > ge)) {
      this.maybeSetVerdict("shift")
      that.maybeSetVerdict("shift")
      return
    }
  }
  def doVerdicts(that: Feat): Unit = {
    if (!this.eq(that)) {
      return
    }
    val ts = this.getStart //tblfeat
    val te = this.getEnd
    val gs = that.getStart //gbkfeat
    val ge = that.getEnd
    //println("cmp ts%d te%d gs%d ge%d" format (ts, te, gs, ge))

    if (math.abs(ts - gs) <= 9 &&
        math.abs(te - ge) <= 9) {
      this.maybeSetVerdict("identity")
      that.maybeSetVerdict("identity")
      return
    }

    var relstart = RelPos.SAME
    if (ts < gs) {
      relstart = RelPos.UP
      //println("relstart UP")
    } else if (ts > gs) {
      relstart = RelPos.DOWN
      //println("restart DOWN")
    }

    var relend = RelPos.SAME
    if (te < ge) {
      relend = RelPos.UP
      //println("relend UP")
    } else if (te > ge) {
      relend = RelPos.DOWN
      //println("relend DOWN")
    }

    //println("relstart: %s, relend %s" format (relstart, relend))

    if ((relstart == RelPos.DOWN && relend == RelPos.SAME) ||
        (relstart == RelPos.SAME && relend == RelPos.UP  ) ||
        (relstart == RelPos.DOWN && relend == RelPos.UP  )) {
      //println("got sub")
      this.maybeSetVerdict("subfeature")
      that.maybeSetVerdict("superfeature")
      return
    }
    if ((relstart == RelPos.UP   && relend == RelPos.SAME) ||
        (relstart == RelPos.SAME && relend == RelPos.DOWN) ||
        (relstart == RelPos.UP   && relend == RelPos.DOWN)) {
      //println("got sup")
      this.maybeSetVerdict("superfeature")
      that.maybeSetVerdict("subfeature")
      return
    }

    if ((relstart == RelPos.UP   && relend == RelPos.UP   && te >= gs) ||
        (relstart == RelPos.DOWN && relend == RelPos.DOWN && ts <= ge)) {
      this.maybeSetVerdict("overlap")
      that.maybeSetVerdict("overlap")
      return
    }
    if ((relstart == RelPos.UP   && relend == RelPos.UP   && te < gs) ||
        (relstart == RelPos.DOWN && relend == RelPos.DOWN && ts > ge)) {
      this.maybeSetVerdict("shift")
      that.maybeSetVerdict("shift")
      return
    }
  }
}
class FeatList {
  var feats = List.empty[Feat]
  def getFeats: List[Feat] = return feats

  def append(f: Feat): Unit = {
    feats ::= (f)
  }
  def doVerdicts(gbkfeats: FeatList): Unit = {
    for (tblfeat <- feats) {
      for (gbkfeat <- gbkfeats.getFeats) {
        tblfeat.doIdentity(gbkfeat)
      }
    }
    for (tblfeat <- feats) {
      for (gbkfeat <- gbkfeats.getFeats) {
        tblfeat.doSubSuper(gbkfeat)
      }
    }
    for (tblfeat <- feats) {
      for (gbkfeat <- gbkfeats.getFeats) {
        tblfeat.doOverlap(gbkfeat)
      }
    }
    for (tblfeat <- feats) {
      for (gbkfeat <- gbkfeats.getFeats) {
        tblfeat.doShift(gbkfeat)
      }
    }
  }
  def detectCopies(gbkfeats: FeatList): Unit = { // after all doVerdicts, change "shift" to "copy" if an "idnetity"/"subfeature"/"superfeature"/"overlap" exists for the rfam id
    def maybeSetCopy(tblfeat: Feat): Unit = {
      for (gbkfeat <- gbkfeats.getFeats) {
        if (tblfeat.eq(gbkfeat) &&
            gbkfeat.getVerdict != null &&
            (gbkfeat.getVerdict.equals("identity") ||
             gbkfeat.getVerdict.equals("subfeature") ||
             gbkfeat.getVerdict.equals("superfeature") ||
             gbkfeat.getVerdict.equals("overlap"))) {
          tblfeat.setVerdict("copy")
          return
        }
      }
    }
    for (tblfeat <- feats) {
      if (tblfeat.getVerdict.equals("shift")) {
        maybeSetCopy(tblfeat)
      }
    }
  }
}
class FeatMap {
  var feats = TreeMap[String, FeatList]()
  def getFeats: TreeMap[String, FeatList] = return feats

  def append(f: Feat): Unit = {
    feats.get(f.getId) match {
      case Some(featlist) => featlist.append(f)
      case None => {
        val featlist = new FeatList
        featlist.append(f)
        feats += (f.getId -> featlist)
      }
    }
  }
}


class CmsearchCmpGenBank(_tblfile: File, _gbkfile: File) {
  var tblfile = _tblfile
  var gbkfile = _gbkfile
  var fnafile: File = null
  var gbkfeats: FeatMap = null
  var tblfeats: FeatMap = null

/*
  private def _parsetbls(dir: File): Boolean = {
    println("attempt %s" format dir.getPath)
    val files = dir.listFiles
    if (files == null) {
      println("is null")
      return true
    }
    for (f <- files) {
      if (f.isDirectory) {
        println("dir %s" format f.getPath)
        if (!_parsetbls(f)) {
          return false
        }
      } else {
        println("file %s" format f.getPath)
        if (f.getPath.endsWith(".tblout")) {
          println("parsing %s" format f.getPath)
          if (!parsetbl(f)) {
            println("Bad parse %s" format f.getPath)
            return false
          }
        }
      }
    }
    return true
  }

  def parsetbls: Boolean = {
    tblfeats = new FeatMap
    return _parsetbls(tbldir)
  }
*/
  def parsetbl: Boolean = {
/*example of rfam.out from runInfernal.pl (Infernal.pm)
A471O8DRAFT_contig_0_0.1	INFERNAL 1.0.2 (October 2009)	misc_RNA	32529	32623	20.0	-	0	ID=A471O8DRAFT_contig_0_0.1.RFAM.5; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00349; Model=snoR11; product=Small nucleolar RNA R11/Z151
A471O8DRAFT_contig_0_0.1	INFERNAL 1.0.2 (October 2009)	misc_RNA	69402	69452	21.0	-	0	ID=A471O8DRAFT_contig_0_0.1.RFAM.8; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01139; Model=sR2; product=Small nucleolar RNA sR2
A471O8DRAFT_contig_10_0.11	INFERNAL 1.0.2 (October 2009)	misc_RNA	51047	51328	124.3	-	0	ID=A471O8DRAFT_contig_10_0.11.RFAM.6; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00373; Model=RNaseP_arch; product=Archaeal RNase P
A471O8DRAFT_contig_2_0.3	INFERNAL 1.0.2 (October 2009)	misc_RNA	10162	10228	23.3	+	0	ID=A471O8DRAFT_contig_2_0.3.RFAM.3; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00154; Model=SNORD63; product=Small nucleolar RNA SNORD63
A471O8DRAFT_contig_2_0.3	INFERNAL 1.0.2 (October 2009)	misc_RNA	50017	50079	22.7	+	0	ID=A471O8DRAFT_contig_2_0.3.RFAM.10; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01666; Model=rox2; product=Drosophila rox2 ncRNA
A471O8DRAFT_contig_3_0.4	INFERNAL 1.0.2 (October 2009)	misc_RNA	147971	147993	21.3	+	0	ID=A471O8DRAFT_contig_3_0.4.RFAM.7; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01079; Model=RF_site3; product=Putative RNA-dependent RNA polymerase ribosomal frameshift site
*/
/* sample tblout from cmsearch
#target name           accession query name                    accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#--------------------- --------- ----------------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
sludgePhrap_Contig3440 -         Betaproteobacteria_toxic_sRNA RF02278    cm        1       64      623      559      -    no    1 0.49   3.3   64.3   1.7e-13 !   -
sludgePhrap_Contig3440 -         Betaproteobacteria_toxic_sRNA RF02278    cm        1       64      623      559      -    no    1 0.49   3.3   64.3   1.7e-13 !   -
#
# Program:         cmsearch
# Version:         1.1rc1 (June 2012)
# Pipeline mode:   SEARCH
# Query file:      /global/homes/s/schaumbe/models/089f
# Target file:     /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/2000000000.fna
# Option settings: cmsearch -o /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/cmsearch/089f/cmsearch.out --tblout /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/cmsearch/089f/cmsearch.tblout --cut_ga --cpu 4 /global/homes/s/schaumbe/models/089f /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/2000000000.fna 
# Current dir:     /global/u1/s/schaumbe
# Date:            Sat Nov 17 18:17:38 2012
# [ok]
*/
    
    tblfeats = new FeatMap
    val tblines = Source.fromFile(tblfile).getLines
    val rfampat = "RNA_Class_ID=([^;]+);".r
    while (tblines.hasNext) {
      val line = tblines.next
      if (!line.startsWith("#")) {
        //not a comment, so got bases, probably.  may be empty line.
        val fields = line.trim.split("""\t+""")
        val seq_from    = fields(3).toInt // feat start
        val seq_to      = fields(4).toInt // feat end
        //val strand    = fields(6)

        /*val note        = fields(8) // feat id
        var rfam_acc: String = null;
        rfampat.findFirstIn(note) match {
          case Some(rfamstr) => rfam_acc = rfamstr
          case None => throw new Exception("cannot extract rfam accession from line: " + line)
        }*/
        val rfam_acc        = fields(8).split(";")(2).split("=")(1) // feat id

        
        //println("tblfeat %s %d %d" format (query_acc, seq_from, seq_to))

        tblfeats.append(new Feat(rfam_acc,
                                 math.min(seq_from, seq_to),
                                 math.max(seq_from, seq_to)))
      }
    }
    return true
  }
  def parsegbk: Boolean = {
    // http://biojava.org/wiki/BioJava:Cookbook:Annotations:List2
    // http://biojava.org/wiki/BioJava:BioJavaXDocs#Qualifiers_as_annotations.
    //val richSeq = RichSequence.IOTools.readGenbankDNA(new BufferedReader(new FileReader(fileName)),null).nextRichSequence()
    val richseqs = RichSequence.IOTools.readGenbankDNA(new BufferedReader(new FileReader(gbkfile.getPath)),null)
    //val sequences = SeqIOTools.readGenbank(new BufferedReader(new FileReader(gbkfile.getPath)))

    var feats = new FeatMap

    while (richseqs.hasNext) {
      val richseq: RichSequence = richseqs.nextRichSequence
      val featitr: Iterator[Feature] = richseq.getFeatureSet.iterator
      while (featitr.hasNext) {
        val richfeat: RichFeature = featitr.next.asInstanceOf[RichFeature] // casts feature to richfeature
        //println("feat %s" format richfeat)
        var from_infernal = false
        var rfam_id: String = null
        for (i <- richfeat.getNoteSet.iterator) {
            val note  = i.asInstanceOf[Note]
            val key   = note.getTerm.getName
            val value = note.getValue
            val rank  = note.getRank
            // print the qualifier out in key=value (rank) format
            //System.out.println(key+"="+value+" ("+rank+")"); 
/*
feat (#17) lcl:A040L21DRAFT_contig_9_0.10/unknown.0:misc_RNA,GenBank(11547..11974)
locus_tag=A040L21DRAFT_00179 (1)
RNA_Class_ID=RF00010 (2)
product=Bacterial RNase P class A (3)
gene_calling_method=INFERNAL 1.0.2 (October 2009) (4)
Model=RNaseP_bact_a (5)
*/            
            if (key == "gene_calling_method") {
              if (value.startsWith("INFERNAL")) {
                from_infernal = true
              }
            } else if (key == "RNA_Class_ID") {
              rfam_id = value
            }
        }
        if (from_infernal && rfam_id != null) {
          val loc = richfeat.getLocation
          //println("infernal feat: %s, %d-%d" format (rfam_id, loc.getMin, loc.getMax))
          //feats += (rfam_id -> new Feat(rfam_id, loc.getMin, loc.getMax))
          feats.append(new Feat(rfam_id, loc.getMin, loc.getMax))
        }
      }
    }
    gbkfeats = feats
    return true
  }
  def derivefna: Boolean = {
    this.fnafile = new File("%s.fna" format gbkfile.getPath)
    val fna_handle = new PrintWriter(this.fnafile)

    val sequences = SeqIOTools.readGenbank(new BufferedReader(new FileReader(gbkfile.getPath)))
    val fasta_chars_per_line = 60
    while (sequences.hasNext) {
      val sequence = sequences.nextSequence

      val s = sequence.seqString
      if (s.length > 0) {
        fna_handle.println(">%s" format sequence.getName)

        var i = 0
        while(i < s.length) {
          val end_idx = math.min(i + fasta_chars_per_line, s.length)
          fna_handle.println(s.substring(i, end_idx))
          i = end_idx
        }
      }
    }
    fna_handle.close
    return true
  }
  def massagegbk: Unit = {
    // bioperl doesn't output LOCUS date, so fake it for biojava
    // http://www.biojava.org/pipermail/biojava-l/2002-January/002013.html
    val gbkfile_massage_path = "%s.ncbi_fmt" format gbkfile.getPath
    val gbkfile_massage = new File(gbkfile_massage_path)
    val gbkfile_massage_handle = new PrintWriter(gbkfile_massage)
    for (line <- Source.fromFile(gbkfile).getLines) {
      if (line.startsWith("LOCUS") && line.endsWith(" ")) {
        gbkfile_massage_handle.println("%s%s" format (line, "01-JAN-1990"))
      } else {
        gbkfile_massage_handle.println(line)
      }
    }
    gbkfile_massage_handle.close
    gbkfile = gbkfile_massage // use massaged gbk now
  }
  def run: Unit = {
    massagegbk
    if (tblfile == null) {
      if (!derivefna) {
        //derive fna from gbk
        println("derive")
        sys.exit(1)
      }
    }
    if (!parsegbk) {
      println("Cannot parse features from gbk file %s" format gbkfile.getPath)
      sys.exit(1)
    }
    if (!parsetbl) {
      println("Cannot parse features from tbl file %s" format tblfile.getPath)
      sys.exit(1)
    }

    //now compare gbkfeats to tblfeats
    /*
    for ((tblfeatid, tblfeatlist) <- tblfeats.getFeats) {
      for (tblfeat <- tblfeatlist.getFeats) {
        for (gbkfeatlist <- gbkfeats.getFeats.get(tblfeatid)) {
          for (gbkfeat <- gbkfeatlist.getFeats) {// look for all idenitites, then all subs/supers, then all overlaps, then all shifts, then assing missing/novel. 
            tblfeat.doVerdicts(gbkfeat)
          }
        }
      }
    }
    */
    for ((tblfeatid, tblfeatlist) <- tblfeats.getFeats) {
      for (gbkfeatlist <- gbkfeats.getFeats.get(tblfeatid)) {
        tblfeatlist.doVerdicts(gbkfeatlist)
      }
    }

    //now print out verdicts
    for ((gbkfeatid, gbkfeatlist) <- gbkfeats.getFeats) {
      for (gbkfeat <- gbkfeatlist.getFeats) {
        println("gbkfeat: %s" format gbkfeat.toVerdict("miss"))
      }
    }
    for ((tblfeatid, tblfeatlist) <- tblfeats.getFeats) {
      for (tblfeat <- tblfeatlist.getFeats) {
        println("tblfeat: %s" format tblfeat.toVerdict("novel"))
      }
    }
  }
}
