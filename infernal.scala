package parfam {
  // http://docs.scala-lang.org/overviews/parallel-collections/overview.html
  import scala.collection.parallel._ // multiprocessing within a Hadoop mapper

  // http://stackoverflow.com/questions/2637643/how-do-i-list-all-files-in-a-subdirectory-in-scala
  import org.apache.commons.exec._ // exec'ing cmsearch etc

  import java.io.File // for existence tests, touching
  import java.io.PrintWriter // for writing files
  import scala.io.Source // for reading files

  class GFFrow(_seqid: String, _source: String, _type: String, _start: Int, _end: Int, _score: Double, _strand: String, _phase: Int, _attributes: String) extends Ordered[GFFrow] {
    val seqid      = _seqid      // Infernal.pm runRfam $seq; here, target_name
    val source     = _source
    val gfftype    = _type
    val start      = _start
    val end        = _end
    val score      = _score // NOTE: THIS MUST BE A BITSCORE
    val strand     = _strand
    val phase      = _phase
    val attributes = _attributes

    //def getSeqid: String  = { println("seqid is %s" format seqid); return seqid }
    def getSeqid: String  = return seqid
    def getStart: Int     = return start
    def getEnd:   Int     = return end
    def getScore: Double  = return score // NOTE: THIS MUST BE A BITSCORE
    def getStrand: String = return strand

    def getRegion: (Int, Int) = if (start <= end) (start, end) else (end, start)
    def getRegionWidth: Int = math.abs(end - start)

    def compare(that: GFFrow): Int = {
      val seqcmp = this.seqid.compareTo(that.getSeqid)
      if (seqcmp != 0) {
        return seqcmp
      }

      val strandcmp = this.strand.compareTo(that.getStrand)
      if (strandcmp != 0) {
        return strandcmp
      }

      val startcmp = this.start - that.getStart
      if (startcmp != 0) {
        return startcmp
      }

      return (this.getRegionWidth - that.getRegionWidth)
    }

    override def toString(): String = {
      // this is a line of GFF
      "%s\t%s\t%s\t%d\t%d\t%3.1f\t%s\t%d\t%s" format
        (seqid, source, gfftype, start, end, score, strand, phase, attributes)
    }

    def overlap(that: GFFrow): Int = { // from CommonFunc.pm isOverlapping
      if (!this.strand.equals(that.getStrand)) {
        return 0
      }

      val (s1, e1) = this.getRegion
      val (s2, e2) = that.getRegion
      var overlap = 0

      if (s2 <= s1 && e2 >= e1)             { overlap = e1 - s1 + 1 }
      if (s2 >= s1 && e2 <= e1)             { overlap = e2 - s2 + 1 }
      if (s1 <= s2 && e1 >= s2 && e1 <= e2) { overlap = e1 - s2 + 1 }
      if (s1 >= s2 && s1 <= e2 && e1 >= e2) { overlap = e2 - s1 + 1 }

      return overlap
    }
  }
  class ThreadsafeGFFrows() {
    var rows = List[GFFrow]()
    val id_counter = new java.util.concurrent.atomic.AtomicInteger(0)

    //tree of regions, sorted by something
    //spinlock/mutex for parallel access
    //or just use actors?
    //or just have a synchronized method, which implicitly uses the monitor for this object?  probably most straightforward.

    def prepend(row: GFFrow) = {
      println("hit:\t%s" format row)
      rows.synchronized {
        rows ::= row // prepends, which reverses order
      }
    }
    def make_attributes(seqid: String, infernal_version: String, info: InfernalModelInfo): String = {
      val id = id_counter.incrementAndGet
      "ID=%s.RFAM.%d; Version=%s; RNA_Class_ID=%s; Model=%s; product=%s" format
        (seqid, id, infernal_version,
         info.getAccession, info.getName, info.getDesc)
    }

    private def _resolve_overlaps(sortedrows: Array[GFFrow], i: Int): Unit = {
      for(j <- (i-5) to (i+5)) { 
        //println("i=%d j=%d len=%d" format (i, j, sortedrows.length))
        //println("is=%s js=%s" format (sortedrows(i).getSeqid,sortedrows(j).getSeqid))
        if (j > 0 && j != i && j < sortedrows.length &&
            sortedrows(i) != null && sortedrows(j) != null &&
            sortedrows(i).getSeqid.equals(sortedrows(j).getSeqid) && // XXX: why require same seqid?  getting only best hit for a given seqid?  perhaps de-dup on seqid after all other de-dup (e.g. CRISPRs w/ ncRNAs) is done
            sortedrows(i).overlap(sortedrows(j)) > 15) {
          if        (sortedrows(i).getScore < sortedrows(j).getScore) { // NOTE: THIS MUST BE A BITSCORE
            sortedrows(i) = null // NOTE: LOWER BITSCORE IS LESS GOOD, so discard it
          } else if (sortedrows(i).getScore > sortedrows(j).getScore) { // NOTE: THIS MUST BE A BITSCORE
            sortedrows(j) = null // NOTE: LOWER BITSCORE IS LESS GOOD, so discard it
          } else {
            if (sortedrows(i).getRegionWidth >= sortedrows(j).getRegionWidth) {
              sortedrows(j) = null // discard smaller region, region j no larger
            } else {
              sortedrows(i) = null // discard smaller region, region i is smaller
            }
          }
        }
      }
    }
    def resolve_overlaps(): Array[GFFrow] = {
      // re-reverses order (1st reverse due to ThreadsafeGFFrows.prepend)
      // invokes GFFrow's compare for sort
      val sortedrows = rows.toArray.reverse.sorted

      for(i <- 0 to sortedrows.length - 1) {
        _resolve_overlaps(sortedrows, i)
      }

      var l = List[GFFrow]()
      for(i <- 0 to sortedrows.length - 1) {
        if (sortedrows(i) != null) {
          l ::= sortedrows(i) // reverses order
        }
      }
      //return sortedrows.filter(elem => elem != null) // removes null elements
      return l.toArray.reverse // reversed order again
    }
    def getRows: Array[GFFrow] = {
      rows.toArray.reverse // de-reverses overder reveral from prepend
    }
  }
  class InfernalModelInfo(_name: String, _acc: String, _desc: String, _clen: Int) extends Ordered[InfernalModelInfo] {
    val name = _name
    val acc  = _acc
    val desc = _desc
    val clen = _clen
    var modeltype: String = null

    def compare(that: InfernalModelInfo): Int = acc.compareTo(that.getAccession)

    def getName: String      = return name
    def getAccession: String = return acc
    def getDesc: String      = return desc
    def getClen: Int         = return clen

    def getType: String = {
      // is threadsafe, always returns the same type
      if (modeltype != null) {
        return modeltype
      }

      val desc_low = desc.toLowerCase
      var t = "misc_RNA"
      if (desc.contains("T-box")      || desc.contains("ydaO/yuaA") ||
          desc.contains("L10 leader") || desc.contains("Hfq binding") ||
          desc_low.contains("pseudoknot")) {
        t = "misc_feature"
      } else if (desc.contains("PyrR") || desc.contains("FMN") ||
                 desc.contains("TPP")  || desc.contains("Purine") ||
                 desc.contains("SAM")  || desc.contains("Cobalamin") ||
                 desc_low.contains("riboswitch")) {
        t = "misc_bind"
      } else if (desc.contains("tmRNA")) {
        t = "tmRNA"
      } else if (desc.contains("tRNA")) {
        t = "tRNA"
      }

      modeltype = t
      return modeltype
    }
  }
  object Infernal {
    import java.util.regex.Pattern
    import java.io.{BufferedReader,FileReader}

    val version_pattern = Pattern.compile("""^# Version:\s+(\S.*)$""") // from cmsearch tblout file
    def extract_version(tblout_line: String): Option[String] = {
      val m = version_pattern.matcher(tblout_line)
      if (m.matches) {
        //println("got version %s in %s" format (m.group(1), tblout_line))
        return Some("INFERNAL %s" format m.group(1)) // first matching group
      }
      //println("no version in " + tblout_line)
      return None
    }

    var model_infos = scala.collection.immutable.TreeMap[String, InfernalModelInfo]()
    val model_name_pattern          = Pattern.compile("""^NAME\s+(\S.*)$""") // from rfam model file
    val model_accession_pattern     = Pattern.compile("""^ACC\S*\s+(\S.*)$""")
    val model_description_pattern   = Pattern.compile("""^DESC\s+(\S.*)$""")
    val model_consensus_len_pattern = Pattern.compile("""^CLEN\s+([0-9]+)$""")
    def set_model_info(model_file: File): Boolean = {
      if (!model_file.exists || model_file.length <= 0) {
        println("ERROR: model file nonexistent or empty: %s", model_file.getPath);
        return false
      }

      val reader = new BufferedReader(new FileReader(model_file))
      def _search(pattern: Pattern): String = {
        var line = reader.readLine
        while (line != null) {
          val m = pattern.matcher(line)
          if (m.matches) {
            return m.group(1)
          }
          line = reader.readLine
        }
        return null
      }
      val name = _search(model_name_pattern)
      val acc  = _search(model_accession_pattern)

      def _search_desc_clen(model_name: String): (String, Int) = {
        var desc: String = null
        var clen = -1
        var line = reader.readLine
        while (line != null) {
          val d = model_description_pattern.matcher(line)
          if (d.matches) {
            desc = d.group(1)
          } else {
            val c = model_consensus_len_pattern.matcher(line)
            if (c.matches) {
              clen = c.group(1).toInt
              if (desc == null) {
                desc = name
              }
              return (desc, clen)
            }
          }
          line = reader.readLine
        }
        return (desc, clen)
      }
      val (desc, clen) = _search_desc_clen(name)
      reader.close // don't need to read whole file

      if (name != null && acc != null && desc != null && clen > -1) {
        val info = new InfernalModelInfo(name, acc, desc, clen)
        model_infos += (acc -> info)
      } else {
        println("ERROR: cannot parse model file: %s", model_file.getPath);
        return false
      }
      return true
    }
    def get_model_info(model_accession: String): InfernalModelInfo = {
      model_infos.get(model_accession) match {
        case Some(model_info) => return model_info
        case None => {
          throw new Exception("ERROR: no such model for accession %s")
          return null
        }
      }
    }
  }
  /*
  object InfernalManagerSignal extends Enumeration {
    val WAIT, COMPLETE = Value
  }
  class InfernalManager() extends Actor {
    def act {
      var mainthread: OutputChannel[Any] = null
      var exitcodes_or = 0
      while (true) {
        receive {
          case InfernalManagerSignal.WAIT => {
            println("waiting")
            mainthread = sender
          }
        }
        if (mainthread != null) {
          println("exiting")
          mainthread ! (InfernalManagerSignal.COMPLETE, exitcodes_or)
          exit()
        }
      }
    }
  }
  */
  class Infernal() {
    import java.io.{BufferedReader,FileReader}
    private def validateOutfile(path: String, last_line_template: String): Boolean = {
      val f = new File(path)
      if (!f.exists || f.length <= 0) {
        println("ERROR: %s wasn't created", path);
        return false // output file not written, so abort
      }

      //last line should be "ok" (as defined by template),
      //check for that
      var last_line: String = null
      val reader = new BufferedReader(new FileReader(f))
      var line = reader.readLine
      while (line != null) {
        last_line = line
        line = reader.readLine
      }
      reader.close

      if (last_line != last_line_template) {
        println("ERROR: %s not completely written", path);
        return false // output file not completely written ok, so abort
      } // else ok so far

      return true
    }
    private def parse_cmsearch_tblout(maybe_gff_rows: Option[ThreadsafeGFFrows], cmsearch_tbloutfile_path: String, maybe_cmsearch_tabfile_path: Option[String]): Boolean = {
      val write_tabfile = (maybe_cmsearch_tabfile_path != None)
      val write_gff = (maybe_gff_rows != None)
      //println("writegff is " + write_gff)
      val gff_rows: ThreadsafeGFFrows = maybe_gff_rows.getOrElse(null)
      val cmsearch_tabfile_path: String = maybe_cmsearch_tabfile_path.getOrElse(null)

      // now convert cmsearch 1.1 tabular out to cmsearch 1.0 tabular out.
/* cmsearch 1.1 tabular format
#target name            accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#---------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
sludgePhrap_Contig4094  -         5S_rRNA              RF00001    cm        1      119      493      607      +    no    1 0.61   0.7   84.6   6.8e-17 !   -
sludgePhrap_Contig4094  -         5S_rRNA              RF00001    cm        1      119      493      607      +    no    1 0.61   0.7   84.6   6.8e-17 !   -
...
*/
/* cmsearch 1.1 tabular format w/ description having whitespace
#target name         accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
hgutS7_s7_161241     -         5S_rRNA              RF00001    cm        1      119      240      356      +    no    1 0.53   0.1   95.0   4.1e-20 !   Human Gut Community Subject 7 : hgutS7_s7_161241
hgutS7_s7_167005     -         5S_rRNA              RF00001    cm        1      119     2386     2501      +    no    1 0.66   5.3   93.0   1.3e-19 !   Human Gut Community Subject 7 : hgutS7_s7_167005
hgutS7_s7_170699     -         5S_rRNA              RF00001    cm        1      119      787      671      -    no    1 0.53   0.1   90.2     7e-19 !   Human Gut Community Subject 7 : hgutS7_s7_170699
*/
/* cmsearch 1.0 tabular format
# CM: purine.2-1
#                                                target coord   query coord                         
#                                      ----------------------  ------------                         
# model name  target name                   start        stop  start   stop    bit sc   E-value  GC%
# ----------  -----------------------  ----------  ----------  -----  -----  --------  --------  ---
  purine.2-1  sludgePhrap_Contig13331        1512        1410      1    103     24.22  2.00e-01   37
  purine.2-1  sludgePhrap_Contig13331        1512        1410      1    103     24.22  2.00e-01   37
...
*/
      val cmsearch_tbloutfile_reader = new BufferedReader(new FileReader(new File(cmsearch_tbloutfile_path)))
      var cmsearch_tabfile_handle: PrintWriter = null
      if (write_tabfile) {
        cmsearch_tabfile_handle = new PrintWriter(new File(cmsearch_tabfile_path))
        cmsearch_tabfile_handle.println("# %32s  %32s  %12s  %12s  %12s  %12s  %8s  %8s  %3s" format (
                                        "model_name", "target_name", "target_start", "target_stop",
                                        "query_start", "query_stop", "bit_sc", "e_value", "gc%"))
      }
      var infernal_version = "UNKNOWN INFERNAL VERSION"
      var hitbuf = List[String]()
      var line = cmsearch_tbloutfile_reader.readLine
      while (line != null) {
        if (line.startsWith("#")) {
          infernal_version = Infernal.extract_version(line).getOrElse(infernal_version)
          //println("infernal version now " + infernal_version)
        } else {
          hitbuf ::= line // reverses order, will re-reverse below
        }
        line = cmsearch_tbloutfile_reader.readLine
      }
      cmsearch_tbloutfile_reader.close

      hitbuf.toArray.reverse.foreach {
        (line: String) => {
          // parse cmsearch 1.1
          //val Array(target_name, target_accession, query_name, query_accession,
          //          mdl, mdl_from, mdl_to, seq_from, seq_to, strand, trunc, pass,
          //          gc_ratio, bias, bit_score, e_value, inc, target_description) =
          //          line.trim.split("""\s+""") // XXX: breaks if whitespace in target_description
          val fields      = line.trim.split("""\s+""")
          val target_name = fields(0)
          val query_name  = fields(2)
          val query_accession = fields(3)
          val mdl_from    = fields(5)
          val mdl_to      = fields(6)
          val seq_from    = fields(7)
          val seq_to      = fields(8)
          val strand      = fields(9)
          val gc_ratio    = fields(12)
          val bit_score   = fields(14)
          val e_value     = fields(15)

          if (write_gff) {
            //println("writing infernal version to gff " + infernal_version)
            val model_info = Infernal.get_model_info(query_accession)
            val attributes = gff_rows.make_attributes(
                               target_name, infernal_version, model_info)
            val row = new GFFrow(target_name, infernal_version,
                                 model_info.getType,
                                 seq_from.toInt, seq_to.toInt,
                                 bit_score.toDouble,
                                 strand, 0, attributes)
            gff_rows.prepend(row)
          }

          if (write_tabfile) {
            // write cmsearch 1.0
            val (start, end) = if (strand.equals("-")) {
              (seq_to, seq_from) 
            } else {
              (seq_from, seq_to)
            }
            cmsearch_tabfile_handle.println("  %32s  %32s  %12s  %12s  %12s  %12s  %8s  %8s  %3d" format
                                            (query_name, target_name,
                                             start, end, mdl_from, mdl_to,
                                             bit_score, e_value,
                                             (gc_ratio.toDouble * 100).toInt))
          }
        }
      }

      if (write_tabfile) {
        cmsearch_tabfile_handle.println("# [ok]")
        cmsearch_tabfile_handle.close

        //is file ok?
        if (!validateOutfile(cmsearch_tabfile_path, "# [ok]")) {
          return false // file bad, so abort
        }
      }
      return true
    }
    private def cmsearch(model_file: File, genome_file: File, cmsearch_concurrency: Int, maybe_gff_file: Option[File], maybe_gff_rows: Option[ThreadsafeGFFrows], resolve_overlaps: Boolean): Int = {
      //set up output directory
      val cmsearch_dir = new File("%s/cmsearch" format genome_file.getParent)
      cmsearch_dir.mkdir

      val model_outdir = new File("%s/%s".format(cmsearch_dir.getPath,
                                                 model_file.getName))
      model_outdir.mkdir

      // description of output files
      // *.cmsearch.out: stdout of cmsearch 1.1
      // *.tblout: tabular output of cmsearch 1.1
      // *.tabfile: tabular output derived from *.tblout, in the format of cmsearch 1.0,
      //            for use with legacy libraries that expect cmsearch 1.0 files
      // *.success: exists if cmsearch stuff completed without error, created here
      val cmsearch_tbloutfile_path = new File("%s/cmsearch.tblout"  format model_outdir.getPath).getPath
      val cmsearch_outfile_path    = new File("%s/cmsearch.out"     format model_outdir.getPath).getPath
      val cmsearch_tabfile_path    = new File("%s/cmsearch.tabfile" format model_outdir.getPath).getPath
      val cmsearch_success_path    = new File("%s/cmsearch.success" format model_outdir.getPath).getPath

      if (new File(cmsearch_success_path).exists &&
          validateOutfile(cmsearch_tbloutfile_path, "# [ok]")) {
        if (parse_cmsearch_tblout(maybe_gff_rows, cmsearch_tbloutfile_path, Some(cmsearch_tabfile_path))) {
          println("cmsearch model " + model_file.getName + " (cached)")
          return 0
        } else {
          //TBD: remove this run and redo cmsearch if issues
          println("cmsearch model " + model_file.getName + " (redoing)")
        }
      } else {
        println("cmsearch model " + model_file.getName + " (running)")
      }

      val commandline = new org.apache.commons.exec.CommandLine("cmsearch") // XXX: write tailrec func to build cmdline from list
      commandline.addArgument("--cpu")
      commandline.addArgument(cmsearch_concurrency.toString)
      commandline.addArgument("--cut_ga")
      commandline.addArgument("--tblout")
      commandline.addArgument(cmsearch_tbloutfile_path)
      commandline.addArgument("-o")
      commandline.addArgument(cmsearch_outfile_path)
      commandline.addArgument(model_file.getPath)
      commandline.addArgument(genome_file.getPath)

      val exec     = new org.apache.commons.exec.DefaultExecutor
      var exitcode = exec.execute(commandline)
      println("cmsearch model " + model_file.getName + " exitcode: " + exitcode)
      if (exitcode != 0) {
        return exitcode // error running cmsearch, so abort
      }

      // are output files ok?
      if (!validateOutfile(cmsearch_outfile_path, "[ok]")) {
        return 1 // file bad, so abort
      }
      if (!validateOutfile(cmsearch_tbloutfile_path, "# [ok]")) {
        return 1 // file bad, so abort
      }

      // for now, always create tabfile, so Some(...).  later, make this optional.
      if (!parse_cmsearch_tblout(maybe_gff_rows, cmsearch_tbloutfile_path, Some(cmsearch_tabfile_path))) {
        return 1
      }

      val f = new File(cmsearch_success_path)
      f.createNewFile // finished, success!  maybe
      if (f.exists) {
        return 0 // succeeded, and success file created indicating that
      } else {
        println("ERROR: %s not created", cmsearch_success_path);
        return 1 // couldn't make success file, so retry.  may have run out of inode quota
      }
    }

    def runModels(model_dir: File, genome_file: File, chunk_div: Int, chunk_id: Int, worker_concurrency: Int, cmsearch_concurrency: Int, maybe_gff_file: Option[File], resolve_overlaps: Boolean): String = {

      /*val mgr = new InfernalManager
      mgr.start
      (mgr !? InfernalManagerSignal.WAIT) match {
        case (InfernalManagerSignal.COMPLETE, exitcodes_or: Int) => {
          //do something with the or'd existcodes.  error?
          println("complete, exitcodes_or %d" format exitcodes_or)
          return ""
        }
        case _ => {
          throw new Exception("Unknown message: " + _)
        }
      }*/

      // sort by filename, in case filesystem doesn't (e.g. NFS)
      var model_filenames = model_dir.listFiles.sorted.map {
        (f: File) => f.getAbsolutePath
      }
      var model_file_count = -1
      try {
        model_file_count = model_filenames.length
      } catch {
        case e: NullPointerException => {
          println("ERROR, models directory inaccessible? " + model_dir)
          throw(e)
        }
      }

      val modelidx_first =
        ((chunk_id)   * model_file_count.toDouble / chunk_div).ceil
      val modelidx_last  =
        ((chunk_id+1) * model_file_count.toDouble / chunk_div).floor

      println("first model idx " + modelidx_first)
      println("last model idx " + modelidx_last)

      // filter out all model files outside first and last model indexes
      model_filenames = model_filenames.zip( // zip makes file,index tuples
                          0 to model_filenames.length - 1).filter( // filter out unwanted indexes
                          x => x._2 >= modelidx_first && x._2 <= modelidx_last).map( // remove indexes, leaving files
                          _._1)

      model_filenames foreach {
        (filename: String) => {
          println("todo model %s" format filename)
          if (!Infernal.set_model_info(new File(filename))) {
            return null // couldn't parse rfam model
          }
        }
      }

      //val model_files = model_paths.split("""\s+""").map(new File(_))
      // parallel collection of all infernal model files

      // http://stackoverflow.com/questions/5424496/scala-parallel-collections-degree-of-parallelism
      //XXX: scala 2.9 concurrency control
      //collection.parallel.ForkJoinTasks.defaultForkJoinPool.setParallelism(worker_concurrency)

      val model_par = model_filenames.par

      //XXX: scala 2.10 concurrency control
      model_par.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(worker_concurrency))

      val gff = new ThreadsafeGFFrows
      val maybe_gff = if (maybe_gff_file == None) {None} else {Some(gff)}
      val exitcodes = model_par.map {
        (model_filename: String) => {
          cmsearch(new File(model_filename), genome_file, cmsearch_concurrency,
                   maybe_gff_file, maybe_gff, resolve_overlaps)
        }
      }
      
      val exitcodes_str = exitcodes.mkString(",")
      val exitcodes_or = exitcodes.foldLeft(0) {
        (accumulated: Int, exitcode: Int) => {
          accumulated | exitcode
        }
      }
      if (exitcodes_or != 0) {
        println("ERROR: at least one cmsearch failed")
        return exitcodes_str
      }

      maybe_gff_file match {
        case Some(f: File) => {
          val gff_handle = new PrintWriter(f)
          gff_handle.println("##gff-version 3")

          val rows = if (resolve_overlaps) {
            gff.resolve_overlaps
          } else {
            gff.getRows
          }
          rows.foreach {
            (row: GFFrow) => {
              gff_handle.println(row)
            }
          }

          gff_handle.println("# [ok]")
          gff_handle.close
        }
        case None => {}
      }

      return exitcodes_str
    }
    def runModel(model: File, genome_file: File, cmsearch_concurrency: Int): Int = {
      val exitcode = cmsearch(model, genome_file, cmsearch_concurrency, None, None, false)
      return exitcode
    }
  }
}
