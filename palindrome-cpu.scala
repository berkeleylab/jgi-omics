package parfam {
  import java.io.File
  
  object PalindromeCPU { // Called PalindromeCPU because an OpenCL/GPU variant is expected.
    val palindrome_cpu_usage = """
Usage:
java -jar omics.jar pal --file my_fasta.fna [--loop 16] [--wobble]

Detects stem-loop motifs in the nucleotide FASTA file my_fasta.fna,
where the stem is a palindromic pair of sequences flanking a loop region.

--loop: The maximum loop length is by default 16 nucleotides.

--wobble: Allow G-U or G-T wobble basepairs for palindrome detection.
          By default, only Watson-Crick basepairs allowed (A-T, A-U, G-C).

No mismatches or indels are tolerated in the stem.
"""
    def usage(msg: String): Unit = {
      if (msg != "") {
        println(msg)
      }
      println(palindrome_cpu_usage)
      sys.exit(1)
    }
    def runArgs(args: Array[String]): Unit = {
      if (args.length < 2) {
        usage("Too few args")
      }
      var badarg = false
      var infilename: String = null
      var max_loop: Int = 16
      var wobble: Boolean = false
      def nextOption(list: List[String]) : Unit = {
        list match {
          case Nil => {}
          case "--file" :: value :: tail => {
            infilename = value
            nextOption(tail)
          }
          case "--loop" :: value :: tail => {
            max_loop = value.toInt
            nextOption(tail)
          }
          case "--wobble" :: tail => {
            wobble = true
            nextOption(tail)
          }
          case option :: tail => {
            println("Unknown option: " + option) 
            badarg = true
            nextOption(tail)
          }
        }
      }
      nextOption(args.toList)
      
      if (badarg) {
        usage("")
      }

      if (infilename == null) {
        usage("Please specify a --file")
      }
      val infile = new File(infilename)
      if (!infile.exists) {
        usage("Input --file doesn't exist")
      }

      if (max_loop < 0) {
        usage("Specify 0 or greater via --loop")
      }

      (new PalindromeCPU(infile, max_loop, wobble)).run()
    }
  }
  class PalindromeCPU(_infile: File, _max_loop: Int, _wobble: Boolean) {
    import scala.annotation.tailrec
    import scala.io.Source // for reading files
    import java.lang.StringBuilder

    val infile = _infile
    val max_loop = _max_loop
    val wobble = _wobble

    def is_watsoncrick_basepair(a: Char, b: Char): Boolean = {
      if (b == 'a' || b == 'A' || b == 'c' || b == 'C' ||
          b == 'g' || b == 'G' || b == 't' || b == 'T' ||
          b == 'u' || b == 'U') {
        a match {
          case 'a'|'A'         => return (b == 't' || b == 'T' || b == 'u' || b == 'U')
          case 'c'|'C'         => return (b == 'g' || b == 'G')
          case 'g'|'G'         => return (b == 'c' || b == 'C')
          case 't'|'T'|'u'|'U' => return (b == 'a' || b == 'A')
          case 'n'|'N'|'x'|'X'|'#' => return false
          case _               => throw new Exception("unrecognized nucleotide: " + a)
        }
      } else if (b == 'n' || b == 'N' ||
                 b == 'x' || b == 'X' || 
                 b == '#') {
        return false
      } else {
        throw new Exception("unrecognized nucleotide: " + b)
      }
    }
    def is_wobble_basepair(a: Char, b: Char): Boolean = {
      if (b == 'a' || b == 'A' || b == 'c' || b == 'C' ||
          b == 'g' || b == 'G' || b == 't' || b == 'T' ||
          b == 'u' || b == 'U') {
        a match {
          case 'a'|'A'         => return (b == 't' || b == 'T' || b == 'u' || b == 'U')
          case 'c'|'C'         => return (b == 'g' || b == 'G')
          case 'g'|'G'         => return (b == 'c' || b == 'C' || b == 't' || b == 'T' || b == 'u' || b == 'U')
          case 't'|'T'|'u'|'U' => return (b == 'a' || b == 'A' || b == 'g' || b == 'G')
          case 'n'|'N'|'x'|'X'|'#' => return false
          case _               => throw new Exception("unrecognized nucleotide: " + a)
        }
      } else if (b == 'n' || b == 'N' ||
                 b == 'x' || b == 'X' || 
                 b == '#') {
        return false
      } else {
        throw new Exception("unrecognized nucleotide: " + b)
      }
    }
    def is_basepair(a: Char, b: Char): Boolean = {
      // use function pointer instead of an if test in here
      val basepair = if (wobble) {
        is_wobble_basepair(a,b)
      } else {
        is_watsoncrick_basepair(a,b)
      }
      return basepair
    }

    @tailrec private def _depth_pal(contig: String, starting_offset: Int, pal_offset: Int, depth: Int): Int = {
      val a_off = starting_offset - depth
      val b_off = pal_offset + depth
      if (a_off < 0 || b_off >= contig.length ||
          !is_basepair(contig.charAt(a_off),contig.charAt(b_off))) {
        return (depth - 1)
      } else {
        return _depth_pal(contig, starting_offset, pal_offset, depth + 1)
      }
    }
    def depth_pal(contig: String, starting_offset: Int, pal_offset: Int): Int = {
      return _depth_pal(contig, starting_offset, pal_offset, 1)
    }
    def find_tail(contig: String, tail_offset: Int, direction: Int): Option[(Char, Int)] = {
      require(direction == 1 || direction == -1)

      if (tail_offset < 0 || tail_offset >= contig.length) {
        return None
      }
      val nucleotide = contig.charAt(tail_offset)

      val start = tail_offset + direction
      if (start < 0 || start >= contig.length) {
        return None
      }
      val end   = (if (direction == 1) {contig.length - 1} else {0})

      var tail_len = 0
      for(i <- start to end by direction) {
        if(contig.charAt(i) == nucleotide) {
          tail_len += 1
        } else {
          if (tail_len <= 1) {
            return None
          } else {
            return Some(nucleotide,tail_len)
          }
        }
      }

      if (tail_len <= 1) {
        return None // a poly-nucleotide tail towards the end of the operon (probably poly-T/tyrosine) must have more than 1 base to be 'poly'.  plenty of false stem-loop motifs without this poly-nucleotide tail constraint
      } else {
        return Some(nucleotide,tail_len)
      }
    }
    def best_pal(contig: String, starting_offset: Int): Option[(Int, Int, Option[(Char, Int)], Option[(Char, Int)])] = {
      var best_pal_offset = -1
      var best_pal_depth  = -1
      var best_head: Option[(Char, Int)] = None
      var best_tail: Option[(Char, Int)] = None

      val a = contig.charAt(starting_offset)
      for(pal_offset <- (starting_offset + 1) to math.min(contig.length - 1, starting_offset + max_loop)) {
        val b = contig.charAt(pal_offset)
        if (is_basepair(a,b)) {
          val pal_depth = depth_pal(contig, starting_offset, pal_offset)
          if (pal_depth > best_pal_depth) {
            val stemloop_start = starting_offset - pal_depth
            val stemloop_end   = pal_offset + pal_depth 
            val head = find_tail(contig, stemloop_start, -1)
            val tail = find_tail(contig, stemloop_end,    1)
            if (head != None || tail != None) { //requires at lesat one poly-nucleotide sequence flanking palindrome to be a real rho-independent terminator motif
              best_pal_offset = pal_offset
              best_pal_depth  = pal_depth
              best_head = head
              best_tail = tail
            }
          }
        }
      }

      if (best_pal_offset == -1) {
        return None
      } else {
        return Some((best_pal_offset, best_pal_depth, best_head, best_tail))
      }
    }
    def print_motif(contig: String, stemloop_start: Int, stem_end: Int, stem_start: Int, stemloop_end: Int,
                    motif_head: Option[(Char, Int)], motif_tail: Option[(Char, Int)]): Unit = {
      val loop_length = stem_start - stem_end - 1 // -1 because loop is only bases between the stems
      def motif_to_str(motif: Option[(Char, Int)]): String = {
        return motif match {
          case Some((nucleotide, len)) => new String(Array.fill(len)(nucleotide))
          case None => ""
        }
      }
      println("stemloop_start %d stem_end %d stem_start %d stemloop_end %d stem_length %d loop_length %d : <%s>[%s](%s)[%s]<%s>".format(
              stemloop_start + 1, // in biology, the first base is referred to as base 1, not base 0, hence the +1 here.  printing the biology numbering is important for comparison to RegulonDB, which refers to the first base as base 1.
              stem_end + 1,
              stem_start + 1,
              stemloop_end + 1, // and the last base would be at contig.length in biology, whereas contig.length-1 is the last character in the contig String
              stem_end - stemloop_start + 1, // +1 because length is inclusive on the ends
              loop_length,
              motif_to_str(motif_head),
              contig.substring(stemloop_start, stem_end+1), // +1 because end in contig.substring(start, end) is exclusive not inclusive
              if (loop_length > 0) {contig.substring(stem_end+1, stem_start)} else {""}, // only print bases between the stems, so +1
              contig.substring(stem_start, stemloop_end+1), // +1 because end in contig.substring(start, end) is exclusive not inclusive
              motif_to_str(motif_tail)))
    }
    def find_stemloop(contig: String): Unit = {
      var uniq_stemloop_start = -1
      var uniq_stem_end       = -1
      var uniq_stem_start     = -1
      var uniq_stemloop_end   = -1
      var uniq_motif_head: Option[(Char, Int)] = None
      var uniq_motif_tail: Option[(Char, Int)] = None

      var stemloop_start = -1
      var stem_end       = -1
      var stem_start     = -1
      var stemloop_end   = -1
      var motif_head: Option[(Char, Int)] = None
      var motif_tail: Option[(Char, Int)] = None

      for(starting_offset <- 0 to contig.length - 1) {
        best_pal(contig, starting_offset) match {
          case Some((pal_offset, pal_depth, head, tail)) => {
            stemloop_start = starting_offset - pal_depth
            stem_end       = starting_offset
            stem_start     = pal_offset
            stemloop_end   = pal_offset + pal_depth
            motif_head = head
            motif_tail = tail

/* without the conditionals below, many redundant/overlapping palindromes are found.
   the best one's the last one of a given stemloop_start/stemloop_end, as the loop submotif is minimized.
   bases will pair if they can pair, forming a stem.
   note the 53/64 case below, as an example.
...
stemloop_start 44 stem_end 47 stem_start 54 stemloop_end 57
stemloop_start 44 stem_end 48 stem_start 53 stemloop_end 57
stemloop_start 44 stem_end 49 stem_start 60 stemloop_end 65
stemloop_start 47 stem_end 50 stem_start 55 stemloop_end 58
stemloop_start 48 stem_end 51 stem_start 62 stemloop_end 65
stemloop_start 51 stem_end 52 stem_start 56 stemloop_end 57
stemloop_start 52 stem_end 53 stem_start 63 stemloop_end 64
stemloop_start 53 stem_end 54 stem_start 63 stemloop_end 64
stemloop_start 53 stem_end 55 stem_start 62 stemloop_end 64
stemloop_start 53 stem_end 56 stem_start 61 stemloop_end 64
stemloop_start 53 stem_end 57 stem_start 60 stemloop_end 64
stemloop_start 56 stem_end 58 stem_start 63 stemloop_end 65
stemloop_start 58 stem_end 59 stem_start 63 stemloop_end 64
stemloop_start 58 stem_end 60 stem_start 62 stemloop_end 64
...
   with the conditionals below, instead this is reported
...
stemloop_start 44 stem_end 48 stem_start 53 stemloop_end 57
stemloop_start 44 stem_end 49 stem_start 60 stemloop_end 65
stemloop_start 47 stem_end 50 stem_start 55 stemloop_end 58
stemloop_start 48 stem_end 51 stem_start 62 stemloop_end 65
stemloop_start 51 stem_end 52 stem_start 56 stemloop_end 57
stemloop_start 52 stem_end 53 stem_start 63 stemloop_end 64
stemloop_start 53 stem_end 57 stem_start 60 stemloop_end 64
stemloop_start 56 stem_end 58 stem_start 63 stemloop_end 65
stemloop_start 58 stem_end 60 stem_start 62 stemloop_end 64
...
   with poly-tail requirement, this is reported
...
stemloop_start 48 stem_end 48 stem_start 56 stemloop_end 56 stem_length 1 loop_length 7
stemloop_start 49 stem_end 49 stem_start 56 stemloop_end 56 stem_length 1 loop_length 6
stemloop_start 50 stem_end 50 stem_start 56 stemloop_end 56 stem_length 1 loop_length 5
stemloop_start 51 stem_end 51 stem_start 56 stemloop_end 56 stem_length 1 loop_length 4
stemloop_start 52 stem_end 52 stem_start 56 stemloop_end 56 stem_length 1 loop_length 3
stemloop_start 51 stem_end 53 stem_start 69 stemloop_end 71 stem_length 3 loop_length 15
stemloop_start 52 stem_end 54 stem_start 58 stemloop_end 60 stem_length 3 loop_length 3
stemloop_start 54 stem_end 55 stem_start 69 stemloop_end 70 stem_length 2 loop_length 13
stemloop_start 58 stem_end 59 stem_start 74 stemloop_end 75 stem_length 2 loop_length 14
stemloop_start 60 stem_end 60 stem_start 75 stemloop_end 75 stem_length 1 loop_length 14
...
*/
            if (uniq_stemloop_start != -1 && 
                (uniq_stemloop_start != stemloop_start ||
                 uniq_stemloop_end   != stemloop_end)) {
              print_motif(contig, uniq_stemloop_start, uniq_stem_end,
                                  uniq_stem_start, uniq_stemloop_end,
                                  uniq_motif_head, uniq_motif_tail)
            }
            uniq_stemloop_start = stemloop_start
            uniq_stem_end       = stem_end
            uniq_stem_start     = stem_start
            uniq_stemloop_end   = stemloop_end
            uniq_motif_head = head
            uniq_motif_tail = tail

          }
          case None => {
          }
        }
      }
      if (stemloop_start != -1) {
        print_motif(contig, stemloop_start, stem_end,
                            stem_start, stemloop_end,
                            motif_head, motif_tail)
      }
    }
    def find_stemloops(): Unit = {
      val fastalines = Source.fromFile(infile).getLines
      var builder: StringBuilder = null
      while (fastalines.hasNext) {
        val line = fastalines.next
        if (line.startsWith(">")) {
          if (builder != null) {
            val s = builder.toString
            builder = null // garbage collect for large complete genomes that are a single contig
            find_stemloop(s)
          }
          println(line)
          builder = new StringBuilder
        } else if (!line.startsWith("#")) {
          //not a comment, so got bases, probably.  may be empty line.
          val l = line.trim
          builder.append(l)
        }
      }
      if (builder.length > 0) {
        val s = builder.toString
        builder = null
        find_stemloop(s)
      }
    }
    def run(): Unit = {
      println("loop is " + max_loop)
      println("a-a WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('a','a'), is_wobble_basepair('a','a')))
      println("a-c WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('a','c'), is_wobble_basepair('a','c')))
      println("a-g WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('a','g'), is_wobble_basepair('a','g')))
      println("a-t WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('a','t'), is_wobble_basepair('a','t')))
      println("c-a WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('c','a'), is_wobble_basepair('c','a')))
      println("c-c WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('c','c'), is_wobble_basepair('c','c')))
      println("c-g WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('c','g'), is_wobble_basepair('c','g')))
      println("c-t WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('c','t'), is_wobble_basepair('c','t')))
      println("g-a WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('g','a'), is_wobble_basepair('g','a')))
      println("g-c WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('g','c'), is_wobble_basepair('g','c')))
      println("g-g WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('g','g'), is_wobble_basepair('g','g')))
      println("g-t WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('g','t'), is_wobble_basepair('g','t')))
      println("t-a WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('t','a'), is_wobble_basepair('t','a')))
      println("t-c WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('t','c'), is_wobble_basepair('t','c')))
      println("t-g WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('t','g'), is_wobble_basepair('t','g')))
      println("t-t WatsonCrick(%b) Wobble(%b)".format(is_watsoncrick_basepair('t','t'), is_wobble_basepair('t','t')))
      println("will basepair via %s".format(if (wobble) {"wobble"} else {"watsoncrick"}))
      find_stemloops()
    }
  }
}

