package parfam

import java.io.{BufferedReader,File,FileReader,PrintWriter}
import scala.io.Source // for reading files

import scala.collection.JavaConversions._ // use java.util.Iterator from features() within a scala for(...)

import scala.collection.immutable.TreeMap
//import scala.collection.immutable.TreeSet

object CmsearchCmpGff {
  val cmsearch_cmp_gff_usage = """
Usage:
java -jar omics.jar cmgff --reference reference.gff --experimental experimental.gff

Compare gff files for features, by
  identity     : feature in both files with start and stop within 9 nucleotides
  subfeature   : feature in tbl contained within feature in gff
  superfeature : feature in tbl contains feature in gff
  5' overlap   : feature in tbl shifted 5' but still overlaps feature in gff
  3' overlap   : feature in tbl shifted 3' but still overlaps feature in gff
  5' shift     : feature in tbl shifted 5' and doesn't overlap feature in gff
  3' shift     : feature in tbl shifted 3' and doesn't overlap feature in gff
  miss         : feature not present in tbl but present in gff
  novel        : feature present in tbl but not present in gff

A feature is typically an ncRNA, given cmsearch was used to find Rfams.
"""
  def usage(msg: String): Unit = {
    if (msg != "") {
      println(msg)
    }
    println(cmsearch_cmp_gff_usage)
    sys.exit(1)
  }
  def runArgs(args: Array[String]): Unit = {
    if (args.length < 2) {
      usage("Too few args")
    }
    var badarg = false
    var referencefilename: String = null
    var experimentalfilename: String = null
    def nextOption(list: List[String]) : Unit = {
      list match {
        case Nil => {}
        case "--reference" :: value :: tail => {
          referencefilename = value
          nextOption(tail)
        }
        case "--experimental" :: value :: tail => {
          experimentalfilename = value
          nextOption(tail)
        }
        case option :: tail => {
          println("Unknown option: " + option) 
          badarg = true
          nextOption(tail)
        }
      }
    }
    nextOption(args.toList)
    
    if (badarg) {
      usage("")
    }

    if (referencefilename == null) {
      usage("Please specify a --reference r.gff")
    }
    val referencefile = new File(referencefilename)
    if (!referencefile.exists) {
      usage("Given --reference doesn't exist")
    }

    if (experimentalfilename == null) {
      usage("Please specify an --experimental e.gff")
    }
    val experimentalfile = new File(experimentalfilename)
    if (!experimentalfile.exists) {
      usage("Given --experimental doesn't exist")
    }

    (new CmsearchCmpGff(referencefile, experimentalfile)).run
  }
}

//example of a rfam to match up
//
//schaumbe@gpint06:~/proj/omics/gbk/analyze/10005$ grep RF00010 /scratch/schaumbe/t/cmsearch/*/cmsearch.tblout
///scratch/schaumbe/t/cmsearch/0009/cmsearch.tblout:A040L21DRAFT_contig_9_0.10 -         RNaseP_bact_a        RF00010    cm       13      319    11551    11944      +    no    1 0.47   0.6   79.3   1.8e-26 !   -
//schaumbe@gpint06:~/proj/omics/gbk/analyze/10005$ grep -3 RF00010 *gbk
//                     /gene_calling_method="Prodigal V2.50: November, 2010"
//     misc_RNA        11547..11974
//                     /locus_tag="A040L21DRAFT_00179"
//                     /RNA_Class_ID="RF00010"
//                     /product="Bacterial RNase P class A"
//                     /gene_calling_method="INFERNAL 1.0.2 (October 2009)"
//                     /Model="RNaseP_bact_a"

class CmsearchCmpGff(_referencefile: File, _experimentalfile: File) {
  var referencefile = _referencefile
  var experimentalfile = _experimentalfile
  var fnafile: File = null
  var experimentalfeats: FeatMap = null
  var referencefeats: FeatMap = null

  def parsegff(gfffile: File): Option[FeatMap] = {
/*example of rfam.out from runInfernal.pl (Infernal.pm)
A471O8DRAFT_contig_0_0.1	INFERNAL 1.0.2 (October 2009)	misc_RNA	32529	32623	20.0	-	0	ID=A471O8DRAFT_contig_0_0.1.RFAM.5; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00349; Model=snoR11; product=Small nucleolar RNA R11/Z151
A471O8DRAFT_contig_0_0.1	INFERNAL 1.0.2 (October 2009)	misc_RNA	69402	69452	21.0	-	0	ID=A471O8DRAFT_contig_0_0.1.RFAM.8; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01139; Model=sR2; product=Small nucleolar RNA sR2
A471O8DRAFT_contig_10_0.11	INFERNAL 1.0.2 (October 2009)	misc_RNA	51047	51328	124.3	-	0	ID=A471O8DRAFT_contig_10_0.11.RFAM.6; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00373; Model=RNaseP_arch; product=Archaeal RNase P
A471O8DRAFT_contig_2_0.3	INFERNAL 1.0.2 (October 2009)	misc_RNA	10162	10228	23.3	+	0	ID=A471O8DRAFT_contig_2_0.3.RFAM.3; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF00154; Model=SNORD63; product=Small nucleolar RNA SNORD63
A471O8DRAFT_contig_2_0.3	INFERNAL 1.0.2 (October 2009)	misc_RNA	50017	50079	22.7	+	0	ID=A471O8DRAFT_contig_2_0.3.RFAM.10; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01666; Model=rox2; product=Drosophila rox2 ncRNA
A471O8DRAFT_contig_3_0.4	INFERNAL 1.0.2 (October 2009)	misc_RNA	147971	147993	21.3	+	0	ID=A471O8DRAFT_contig_3_0.4.RFAM.7; Version=INFERNAL 1.0.2 (October 2009); RNA_Class_ID=RF01079; Model=RF_site3; product=Putative RNA-dependent RNA polymerase ribosomal frameshift site
*/
/* sample tblout from cmsearch
#target name           accession query name                    accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#--------------------- --------- ----------------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
sludgePhrap_Contig3440 -         Betaproteobacteria_toxic_sRNA RF02278    cm        1       64      623      559      -    no    1 0.49   3.3   64.3   1.7e-13 !   -
sludgePhrap_Contig3440 -         Betaproteobacteria_toxic_sRNA RF02278    cm        1       64      623      559      -    no    1 0.49   3.3   64.3   1.7e-13 !   -
#
# Program:         cmsearch
# Version:         1.1rc1 (June 2012)
# Pipeline mode:   SEARCH
# Query file:      /global/homes/s/schaumbe/models/089f
# Target file:     /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/2000000000.fna
# Option settings: cmsearch -o /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/cmsearch/089f/cmsearch.out --tblout /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/cmsearch/089f/cmsearch.tblout --cut_ga --cpu 4 /global/homes/s/schaumbe/models/089f /global/homes/s/schaumbe/scratch/schaumbe/img_core_v400/2000000000/2000000000.fna 
# Current dir:     /global/u1/s/schaumbe
# Date:            Sat Nov 17 18:17:38 2012
# [ok]
*/
    
    val feats = new FeatMap
    val gfflines = Source.fromFile(gfffile).getLines
    //val rfampat = "RNA_Class_ID=([^;]+);".r
    val rfampat = "RNA_Class_ID=([^;]+);".r
    while (gfflines.hasNext) {
      val line = gfflines.next
      if (!line.startsWith("#")) {
        //not a comment, so got bases, probably.  may be empty line.
        val fields = line.trim.split("""\t+""")

        val note        = fields(8) // feat id
        rfampat.findFirstIn(note) match {
          case Some(rfampat(rfam_acc)) => {
            val seq_from = fields(3).toInt // feat start
            val seq_to   = fields(4).toInt // feat end
            feats.append(new Feat(rfam_acc,
                                  math.min(seq_from, seq_to),
                                  math.max(seq_from, seq_to)))
          }
          case None => {}
        }
      }
    }
    return Some(feats)
  }
  def run: Unit = {
    (parsegff(experimentalfile)) match {
      case Some(feats) => experimentalfeats = feats
      case None => {
        println("Cannot parse features from experimental file %s" format experimentalfile.getPath)
        sys.exit(1)
      }
    }
    (parsegff(referencefile)) match {
      case Some(feats) => referencefeats = feats
      case None => {
        println("Cannot parse features from reference file %s" format referencefile.getPath)
        sys.exit(1)
      }
    }

    //now compare experimentalfeats to referencefeats
    for ((referencefeatid, referencefeatlist) <- referencefeats.getFeats) {
      for (experimentalfeatlist <- experimentalfeats.getFeats.get(referencefeatid)) {
        referencefeatlist.doVerdicts(experimentalfeatlist)
      }
    }
    for ((referencefeatid, referencefeatlist) <- referencefeats.getFeats) {
      for (experimentalfeatlist <- experimentalfeats.getFeats.get(referencefeatid)) {
        experimentalfeatlist.detectCopies(referencefeatlist)
        referencefeatlist.detectCopies(experimentalfeatlist) // just to be consistent, showing copies in ref wrt exp
      }
    }

    //now print out verdicts
    for ((experimentalfeatid, experimentalfeatlist) <- experimentalfeats.getFeats) {
      for (experimentalfeat <- experimentalfeatlist.getFeats) {
        println("experimentalfeat: %s" format experimentalfeat.toVerdict("novel"))
      }
    }
    for ((referencefeatid, referencefeatlist) <- referencefeats.getFeats) {
      for (referencefeat <- referencefeatlist.getFeats) {
        println("referencefeat: %s" format referencefeat.toVerdict("miss"))
      }
    }
  }
}
