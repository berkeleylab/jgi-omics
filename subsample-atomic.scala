package parfam;

import java.util.concurrent.atomic.AtomicInteger
import java.util.StringTokenizer
import java.io.File
import java.io.PrintWriter // for writing files
import java.security.SecureRandom
import java.nio.ByteBuffer
import java.lang.Math
import scala.annotation
import scala.io.Source // for reading files
import scala.collection.mutable
import scala.actors.Actor
import scala.actors.Actor._

object PrinterSignal extends Enumeration {
  val COMPLETE, SHUTDOWN = Value
}

class GeneAtomic(_name: String, _count: Int, _lobound: Int, _hibound: Int) {
  //Gene is used for drawing WITHOUT replacement, so lobound/hibound are
  //adjusted as hits/unhits occur.  For Gene, a single Actor calculates all
  //the hits, then as a second pass a single thread adjusts all the hitcounts
  //to have the correct hit count for the subsample.
  //
  //GeneAtomic, however, is used for drawing WITH replacement, so lobound/
  //hibound are NOT adjusted.  Moreover, many actors concurrently may hit
  //this Gene, so hitcount must be atomic.
  //
  //Finally, it's entirely possible for a GeneAtomic for hitcount > count,
  //which is to say there are more hits than there are genes in this pfam.
  //This is a direct result of drawing WITH replacement.  It's like fishing
  //in a lake, catching a fish, throwing it back in the lake, then catching
  //the same fish again, and saying you caught two fish.

  val name = _name
  val count = _count // count of genes in this pfam
  def getCount: Int = { return count }

  var hitcount = new AtomicInteger(0)
  def getHitcount: Int = { return hitcount.get }

  var lobound = _lobound
  var hibound = _hibound
  def lo: Int = { return lobound }
  def hi: Int = { return hibound }

  def incHitcount: Unit = {
    hitcount.incrementAndGet // don't adjust bounds
  }

  def report: String = {
    return ("%s\t%d" format (name, hitcount.get))
  }
}
class RandRepHitter(_manager: RandRepManager, _hitteridx: Int, _subsample: Float) extends Actor {
  var genes: Array[GeneAtomic] = null
  val manager = _manager
  val hitteridx = _hitteridx
  val subsample = _subsample
  var randi: RandInt = null
  var modulus = -1

  def hits(randi: RandInt, hits_todo: Int): Unit = {
    for (i <- 0 to hits_todo - 1) {
      genes( RandomRepGroup.randGene(genes, randi) ).incHitcount
    }
  }
  def setGenes(_genes: Array[GeneAtomic]): Unit = {
    genes = _genes
  }
  def act {
    while (true) {
      receive {
        case HitterSignal.SHUTDOWN => {
          //println("hitter %d shutdown" format (hitteridx))
          manager ! (hitteridx, HitterSignal.SHUTDOWN) // inform manager of exit
          exit()
        }
        case HitterSignal.COMPLETE => {
          //println("hitter %d done" format (hitteridx))
          manager ! (hitteridx, HitterSignal.COMPLETE) // inform manager of work completion
          genes = null // need new genes to process
          modulus = -1
        }
        case hits_todo: Int => {
          if (modulus == -1) {
            modulus = genes(genes.length - 1).hi
            randi = new RandInt(modulus) // picks a random hit among all gene families (GeneAtomics)
          }
          //println("hitter %d geneidx %d" format (hitteridx, genesidx))
          hits(randi, hits_todo)
          // ask for more work, saying how many samples were taken)
          manager ! (hitteridx, hits_todo)
        }
        case oops => {
          throw new Exception("hitter %d got unexpected data:\n%s"
                              format(hitteridx, oops))
        }
      }
    }
  }
}
class GeneAtomicReader(_genefiles: List[File], _manager: RandRepManager, _genestates: Array[GeneAtomicState]) extends Actor {
  var genefiles = _genefiles
  val manager = _manager
  val genestates = _genestates

  def read(genestatesidx: Int) {
    val genefile = genefiles.head
    genefiles = genefiles.tail
    val (genecount, genes) = RandomRepGroup.geneary(genefile)

    genestates(genestatesidx).setGenes(genecount, genes, genefile)
  }
  def act {
    while(true) {
      receive {
        case (genestatesidx: Int) => {
          read(genestatesidx)
          manager ! (ReaderSignal.COMPLETE, genestatesidx)
        }
        case ReaderSignal.SHUTDOWN => {
          manager ! ReaderSignal.SHUTDOWN
          exit()
        }
        case oops => {
          throw new Exception("reader got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
  }
}
class RandPrinter(_subsample_ratio: Float, _manager: RandRepManager, _genestates: Array[GeneAtomicState]) extends Actor {
  val subsample_ratio = _subsample_ratio
  val manager = _manager
  val genestates = _genestates

  def printGenes(genestatesidx: Int): Unit = {
    var (samples_available, samples_needed, genes, genecount, genefile) =
      genestates(genestatesidx).get

    //XXX: should now print out the results here
    val outfilename = "%s.sub%d" format
      (genefile.getName, (subsample_ratio * 100).toInt)

    //XXX should print samples actual here
    //System.err.println("""samples actual     : %12d
//output file name   : %s""" format
    //  (samples_available, outfilename))

    val outfile = new PrintWriter(outfilename)
    for(gene <- genes) {
      outfile.println(gene.report)
    }
    outfile.close
  }
  def act {
    while(true) {
      receive {
        case genestatesidx: Int => {
          //println("printing.... " + genestatesidx)
          printGenes(genestatesidx)
          //println("printed " + genestatesidx)
          manager ! (PrinterSignal.COMPLETE, genestatesidx)
          //could send signal to a printer object
        }
        case PrinterSignal.SHUTDOWN => {
          manager ! PrinterSignal.SHUTDOWN
          exit()
        }
        case oops => {
          throw new Exception("printer got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
  }
}
class GeneAtomicState() {
  var samples_available = -1
  var samples_needed = -1
  var genes: Array[GeneAtomic] = null
  var genecount = -1
  var genefile: File = null

  def clear: Unit = {
    samples_available = -1
    samples_needed = -1
    genes = null
    genecount = -1
    genefile = null
  }
  def setGenes(_genecount: Int, _genes: Array[GeneAtomic], _genefile: File): Unit = {
    genecount = _genecount
    genes = _genes
    genefile = _genefile
  }
  def getGenes(): (Int, Array[GeneAtomic], File) = {
    return (genecount, genes, genefile)
  }
  def set(_samples_available: Int, _samples_needed: Int, _genes: Array[GeneAtomic], _genecount: Int, _genefile: File): Unit = {
    samples_available = _samples_available
    samples_needed = _samples_needed
    genes = _genes
    genecount = _genecount
    genefile = _genefile
    //println("set genefile to " + _genefile)
    //println("set genefile is " + genefile)
  }
  def get(): (Int, Int, Array[GeneAtomic], Int, File) = {
    //println("genestate available: " + samples_available)
    //println("genestate needed: " + samples_needed)
    //println("genestate genes: " + genes)
    //println("genestate genecount: " + genecount)
    //println("genestate file: " + genefile)
    return (samples_available, samples_needed, genes, genecount, genefile)
  }
}
class RandRepManager(_genefileslen: Int, _genefiles: List[File], _subsample: Float, _num_hitters: Int) extends Actor {
  //var genefiles = _genefiles
  val genefileslen = _genefileslen
  val subsample_ratio = _subsample
  val hitters = new Array[RandRepHitter](_num_hitters)
  var genefilesread = 0
  var genefilesout = 0

  val genestates = new Array[GeneAtomicState](5)
  var genestatesidx_in  = 0 
  //var genestatesidx     = 0
  var genestatesidx_out = 0

  var genestateidx_in_backlog = -1

  val printer = new RandPrinter(subsample_ratio, this, genestates)
  val reader = new GeneAtomicReader(_genefiles, this, genestates)

  def readGeneState(): Unit = {
    if (genefilesread < genefileslen) {
      genefilesread += 1 // count number of read requests
      reader ! genestatesidx_in
      genestatesidx_in = (genestatesidx_in + 1) % genestates.length
    }
  }
  def printerCompleted(genestateidx: Int): Unit = {
    genefilesout += 1 // just record that the gene file completed
    //in this wa, readers don't overflow buffers before writers catch up
  }
  //XXX: don't specify genefile.  actor should have array of them
  def incomingGeneState(): (Int, Array[GeneAtomic], File) = {
    // if not null, sleep???.  no, recieve.  wait in receive for incoming genestatesidx
    //genestates(genestatesidx_in).clear // XXX: actor does this, sets some state
    //receive here to get next incoming state TBD
    //genestatesidx_in = (genestatesidx_in + 1) % genestates.length

    if (genestateidx_in_backlog >= 0) {
      val idx = genestateidx_in_backlog
      genestateidx_in_backlog = -1
      return genestates(idx).getGenes
    } else {
      receive {
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          return genestates(genestateidx).getGenes
        }
        case (PrinterSignal.COMPLETE, genestateidx: Int) => {
          printerCompleted(genestateidx)
        }
        case oops => {
          throw new Exception("manager %d got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    return (0, null, null)
  }
  def outgoingGeneState(samples_available: Int, samples_needed: Int, genes: Array[GeneAtomic], genecount: Int, genefile: File): Unit = {
    //println("d genefile: " + genefile)
    genestates(genestatesidx_out).set(samples_available, samples_needed,
                                      genes, genecount, genefile)
    printer ! genestatesidx_out
    genestatesidx_out = (genestatesidx_out + 1) % genestates.length
  }
  
  var cur_gene: Int = java.lang.Integer.MAX_VALUE // must set to 0
  var cur_gene_max: Int = -1 // must set to positive number
  def runHitter(rhit: RandRepHitter, genes: Array[GeneAtomic]): Unit = {
    var hits_todo = ((cur_gene_max - cur_gene) / (3 * hitters.length)).toInt + 10
    if (hits_todo + cur_gene >= cur_gene_max) {
      hits_todo = cur_gene_max - cur_gene
    }
    if (cur_gene < cur_gene_max) {
      rhit ! hits_todo
    } else if (cur_gene == cur_gene_max) {
      // give hitters the work completion message, but only once
      for (h <- hitters) {
        h ! HitterSignal.COMPLETE
      }
    }
    cur_gene += math.max(hits_todo, 1) // also prevents multiple shutdowns
  }
  def sampleGenes(): Unit = {
    //println("a genefile: " + genefile)
    val (genecount, genes, genefile) = incomingGeneState()
    //println("b genefile: " + genefile)

    System.err.println("input file name    : %s" format
      (genefile.getName))

    val samples_needed = (subsample_ratio * genecount).toInt
    System.err.println("""subsample ratio    : %.10f
gene count         : %12d
samples needed     : %12d""" format
      (subsample_ratio, genecount, samples_needed))

    cur_gene = 0 // for runHitter, to track position
    cur_gene_max = math.ceil(genecount * subsample_ratio).toInt
    for (i <- 0 to hitters.length - 1) {
      hitters(i).setGenes(genes) // hit these genes
    }
    // give hitters a backlog of work, so always busy
    for (i <- 0 to hitters.length - 1) {
      runHitter(hitters(i), genes)
    }
    // balance backlog of work, first hitter gets most hits_todo otherwise
    for (i <- hitters.length - 1 to 0) {
      runHitter(hitters(i), genes)
    }

    var samples_available = 0
    var running_hitters = hitters.length
    while (running_hitters > 0) {
      receive { // assign work as hitters need more
        case (hitteridx: Int, HitterSignal.COMPLETE) => {
          //println("mgr: hitter done: %d" format hitteridx)
          running_hitters -= 1
        }
        case (hitteridx: Int, samples_taken: Int) => {
          //println("mgr: hitter samples: %d" format samples_taken)
          samples_available += samples_taken
          runHitter(hitters(hitteridx), genes)
        }
        case (PrinterSignal.COMPLETE, genestateidx: Int) => {
          printerCompleted(genestateidx)
        }
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          genestateidx_in_backlog = genestateidx // process later
        }
        case oops => {
          throw new Exception("manager got unexpected data:\n%s"
                              format(oops))
        }
      }
    }

    //now print out the results
    val outfilename = "%s.sub%d" format
      (genefile.getName, (subsample_ratio * 100).toInt)
    System.err.println("""samples actual     : %12d
output file name   : %s""" format
      (samples_available, outfilename))

    //println("c genefile: " + genefile)
    outgoingGeneState(samples_available, samples_needed, genes, genecount, genefile)
    //println("done w/ outgoing")

    //do not overflow buffers reading gene files, wait for prints/outgoing to catch up
    while (genefilesread > genefilesout + 1) {
      receive {
        case (PrinterSignal.COMPLETE, genestateidx: Int) => {
          printerCompleted(genestateidx)
        }
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          genestateidx_in_backlog = genestateidx // process later
        }
        case oops => {
          throw new Exception("manager %d got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    readGeneState // request another gene file read
  }
  def act {
    // create gene states buffer for incoming and outgoing gene states
    for (i <- 0 to genestates.length - 1) {
      genestates(i) = new GeneAtomicState
    }

    //spawn genereader, TBD
    reader.start
    readGeneState // read first file
    readGeneState // backlog of work

    //spawn hitters, will reuse
    for (i <- 0 to hitters.length - 1) {
      hitters(i) = new RandRepHitter(this, i, subsample_ratio)
      hitters(i).start
    }

    printer.start

    //process each file full of pfams each having gene counts
    for (i <- 0 to genefileslen - 1) {
//    while(genefiles.nonEmpty) {
//      val genefile = genefiles.head
      //println("genefile: " + genefile)
//      genefiles = genefiles.tail
//      sampleGenes(genefile)
      sampleGenes
    }

    //kill hitters, printer, and reader
    //println("shutting down")
    var live_hitters = hitters.length
    for (i <- 0 to live_hitters - 1) {
      hitters(i) ! HitterSignal.SHUTDOWN // send shutdown message
    }
    var live_printer = true
    printer ! PrinterSignal.SHUTDOWN
    var live_reader = true
    reader ! ReaderSignal.SHUTDOWN
    while (live_printer && live_reader &&
           live_hitters > 0 && genefilesread > genefilesout) {
      receive { // assign work as hitters need more
        case (hitteridx: Int, HitterSignal.SHUTDOWN) => {
          //println("hitter " + hitteridx)
          //println("live " + live_hitters)
          live_hitters -= 1
        }
        case (PrinterSignal.COMPLETE, genestateidx: Int) => {
          // gene file write finished
          printerCompleted(genestateidx)
        }
        case PrinterSignal.SHUTDOWN => {
          live_printer = false
        }
        case ReaderSignal.SHUTDOWN => {
          live_reader = false
        }
        case oops => {
          throw new Exception("manager got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    //println("done")
    exit()
  }
}
object RandomRepGroup {
  def geneary(infile: File): (Int, Array[GeneAtomic]) = {
    val inlist = new mutable.ListBuffer[GeneAtomic]
    val inlines = Source.fromFile(infile).getLines
    var genecount = 0
    while(inlines.hasNext) {
      val tokens = new StringTokenizer(inlines.next)
      val name = tokens.nextToken
      val count = tokens.nextToken.toInt
      val lobound = genecount
      val hibound = genecount + count
      genecount = hibound
      inlist += new GeneAtomic(name, count, lobound, hibound)
    }
    return (genecount, inlist.toArray)
  }

  // binary search for geneid within gene bounds
  def findGene(genes: Array[GeneAtomic], geneid: Int, idx_lo: Int, idx_hi: Int): Int = {
    val idx = idx_lo + ((idx_hi - idx_lo) / 2)
    val gene = genes(idx)
    if (geneid < gene.lo) {
      return findGene(genes, geneid, idx_lo, idx - 1)
    } else if (geneid >= gene.hi) {
      return findGene(genes, geneid, idx + 1, idx_hi)
    } else {
      return idx
    }
  }

  // pick a random gene
  def randGene(genes: Array[GeneAtomic], random: RandInt): Int = {
    val geneid = random.next
    //random.decModulus // accounts for one fewer genes to pick from next time (for WITHOUT REPLACEMENT)
    return findGene(genes, geneid, 0, genes.length - 1)
  }

  val subsample_usage = """
Usage:
java -jar omics.jar randgrpr worker_threads subsample_ratio infile1 infile2 ...

randgrpr takes a random sample of the given subsample_ratio (more than 0.0)
of the given input files.

Subsamples are taken WITH replacement.
For subsamples WITHOUT replacement, use the randgrp command, not randgrpr.

The number of compute-intensive threads to use for cryptographically secure
random number generation is given by worker_threads.
There is a truly random seed for each random number generator.

Input file format:
groupname membercount
groupname membercount
...

Input file example:
A 2
B 0
C 0
D 4

The input file example is equivalent to...
A A D D D D
...and a random subsample is taken of that, with replacement.
"""
  def usage(msg: String): Unit = {
    if (msg != "") {
      println(msg)
    }
    println(subsample_usage)
    sys.exit(1)
  }
  def runArgs(args: Array[String]) {
    if (args.length < 3) {
      usage("Too few args")
    }
    val worker_threads = args(0).toInt
    val subsample_ratio = args(1).toFloat

    if (subsample_ratio <= 0.0) {
      usage("Must specify subsample_ratio greater than 0.0")
    }

    val genefiles = new mutable.ListBuffer[File]
    var genefileslen = 0
    for(i <- 2 to args.length - 1) {
      val genefile = new File(args(i))
      if (!genefile.exists) {
        println("ERROR: File %s does not exist." format args(i))
        System.exit(1)
      }
      genefiles += genefile
      genefileslen += 1
    }

    System.err.println("worker threads     : %12d" format
      (worker_threads))

    val rm = new RandRepManager(genefileslen, genefiles.toList, subsample_ratio, worker_threads)
    rm.start
  }
}
