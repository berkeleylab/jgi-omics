#!/bin/bash -x
#(for f in $(ls /home/ajs/img_core_v400/[0-9]*[0-9]/[0-9]*[0-9].fna | sort -r); do
#  echo $(date +%Y%m%dt%H%M%S) $f
#  time java -jar parfam2.jar --genome $f --model_dir models --cmsearch_threads 4 --worker_threads 2
#done) >& parfam2.out.$(date +%Y%m%dt%H%M%S)

(ls /home/ajs/img_core_v400/[0-9]*[0-9]/[0-9]*[0-9].fna | \
  sort -r | \
  xargs -L 1 -P 8 \
        --no-run-if-empty --verbose \
        -I GENOME \
        bash -c "date +%Y%m%dt%H%M%S; time java -jar parfam2.jar --genome GENOME --model_dir models --cmsearch_threads 1 --worker_threads 1"
) >& parfam2.out.$(date +%Y%m%dt%H%M%S)
