#!/bin/bash
#
# run cmsearch via hadoop jobs over UGE/SGE on genepool
#
# reserves a node, runs some hadoop work on it, frees the node
# does not reserve a whole block of nodes.
# rather, single nodes are reserved, used, and freed.
#
# reserves a bunch of independent nodes in this way, since
# a large tiled/array job consistents of subjobs which may
# start an undefined amount of time, introducing undefined
# amounts of latency, which risks the first job hitting the
# wall time before the last job is run, which means the
# whole job never happens.
#
# comments: aschaumberg@lbl.gov
# 2012

# arguments: $1=genome_file

me=${USER:-$(whoami)} # XXX: only known to work if me is schaumbe
home=/global/homes/$(cut -c 1 <<<$me)/$me
genome_file=${1:-$home/scratch/schaumbe/img_core_v400/2000000000/2000000000.fna}

node_count=20
node_reservation_script=$home/scripts/cmsearch.q-genepool
model_dir=$home/models
worker_processes=2
cmsearch_processes=4
qstat_cache_file=$home/qstat-cache.txt # written by ~/scripts/qstat-cache.sh
max_retries=10 # may retry processing taxon this many times, before giving up

this_script=$0
start_time=$(date +%Y%m%dt%H%M%S)

export genome_basename=$(basename $genome_file)
export genome_dirname=$(dirname $genome_file)

tmp_dir=${TMPDIR:-/tmp}

# prevents concurrenct processes from overloaded qstat
qstat_lock_file=$tmp_dir/$(basename $this_script).$me.lock
touch $qstat_lock_file

#model_count=2208 # known from ls /global/homes/s/schaumbe/models | wc -l
model_count=$(ls $model_dir | wc -l) # 2208 # known from ls /global/homes/s/schaumbe/models | wc -l
model_count=${model_count:-2208}

# timestamped debug messages to stderr
debug() {
  local msg=$1
  echo "$(date +%Y%m%dt%H%M%S) $genome_basename: $msg" 1>&2
}

# get job id from job submission
#schaumbe@gpint06:~/scripts$ qsub ~/scripts/cmsearch.q-genepool 
#Your job 3920323 ("cmsearch.q-genepool") has been submitted
request_node() {
  # XXX: sometimes "run" is returned rather than a job_id.
  # debug this.  print what qsub said, exit, if what would be printed is 'run' (or not a job_id, which is just numeric).
  local qsub_out=$(qsub $node_reservation_script 2>&1)
  local job_id=$(awk '{print $3}' <<<"$qsub_out" | egrep '^[0-9]+$')
  [[ -z "$job_id" ]] && {
    debug "ERROR: couldn't get job id from qsub output: $qsub_out"
    sleep 60 # maybe later these problems will go away.  cmsearch-taxa.sh will invoke another cmsearch-hadoop-all.sh instance upon exit.
    exit 1
  }
  echo $job_id
}

# wait for job reservation to be placed on a node,
# return that node's hostname
get_reservation_hostname() {
  local lock_file=$1 qstat_cache_file=$2 node_count=$3 job_id=$4
  local reservation
  while [[ 1 ]]; do
    # Use flock to prevent qstat from being overloaded
    # Use sleep to only call qstat at most once per second
    #reservation=$(flock $lock_file -c "sleep 1; qstat -s r" 2> /dev/null |egrep "^$job_id")

    # use qstat cache file to further prevent overload.
    # the qstat-cache.sh daemon updates this file every 10 secs or so.
    # use flock to minimize filesystem load from `cat`,
    # assuming many such cat'ers exist, e.g. multiple cmsearch-hadoop-all.sh
    # to process all taxa.
    #reservation=$(flock $lock_file -c "cat $qstat_cache_file" 2> /dev/null | \
    #              egrep "^$job_id" | egrep '\sr\s')

    # flock is overkill.  running out of resources to even fork.
    reservation=$(egrep "^$job_id.*\sr\s" < $qstat_cache_file)
    [[ -n "$reservation" ]] && break

    debug "Waiting for job $job_id to be placed on a node..."
    sleep $node_count # lowering load on `flock`, `cat`, etc
  done

#schaumbe@gpint06:~/scripts$ qstat
#job-ID  prior   name       user         state submit/start at     queue                          jclass                         slots ja-task-ID 
#------------------------------------------------------------------------------------------------------------------------------------------------
#3920310 0.31554 cmsearch.q schaumbe     r     11/16/2012 11:12:42 normal.q@sgi04b20.nersc.gov                                       8 1
#3920310 0.31196 cmsearch.q schaumbe     r     11/16/2012 11:12:42 normal.q@sgi06b19.nersc.gov                                       8 2
  local host=$(awk '{print $8}' <<<$reservation | sed -r 's/.*@//g')
  debug "Job $job_id placed on host $host"
  echo $host
}

# enqueue all hadoop jobs to process all models for a taxon
run_taxon() {
  # run one hadoop on each available node
  now=$(date +%Y%m%dt%H%M%S)
  runs_dir=$(echo $home/runs/$start_time.$now.$genome_basename)
  mkdir -p $runs_dir
  export host_id=0
  while [[ "$host_id" -lt "$node_count" ]]; do
    (
      job_id=$(request_node) # request a node to run on
      host=$(get_reservation_hostname $qstat_lock_file $qstat_cache_file $node_count $job_id)

      run_dir=$runs_dir/$host_id.$host
      hadoop_input=$run_dir/hadoop-input
      hadoop_status=$run_dir/hadoop-status
      hadoop_output=$run_dir/hadoop-output

      mkdir -p $run_dir

      set -x
      echo "$host_id $node_count $genome_file $model_dir $worker_processes $cmsearch_processes" > $hadoop_input

      ssh $host "HADOOP_CLASSPATH=/usr/share/java/hadoop-lzo-0.4.15.jar:$home/scalding/target/scalding-assembly-0.8.2-SNAPSHOT.__PRODUCTION__.jar:$home/job-jars/InfernalJob.__PRODUCTION__.jar hadoop jar $home/scalding/target/scalding-assembly-0.8.2-SNAPSHOT.__PRODUCTION__.jar -libjars $home/job-jars/InfernalJob.__PRODUCTION__.jar -Dmapred.reduce.tasks=20 -Dmapred.min.split.size=2000000000 InfernalJob --hdfs --input $hadoop_input --output $hadoop_output" >& $hadoop_status &
      set +x

      debug "Freeing job $job_id on $host"
      qdel $job_id # free reservation
    ) & # all node reservations and hadoop work are asynchronous
    sleep 1 # don't overload qsub.  qsub bursts may mean jobs get rejected.
    export host_id=$(( $host_id + 1 ))
  done
  wait # for each hadoop
  debug "finished attempt of genome $genome_basename"
}

count_model_successes() { # number of models succesfully completed cmsearch
  ls $genome_dirname/cmsearch/*/cmsearch.success 2> /dev/null | wc -l
}

retry_count=0
model_success_count=$(count_model_successes)
if [[ "${model_success_count}" -lt "${model_count}" ]]; then
  while [[ "${retry_count}" -lt "${max_retries}" ]]; do
    run_taxon # run all models for this organism, may have been run before

    # one cmsearch.success file for every good model run
    # if all such files exist, work's done
    model_success_count=$(count_model_successes)
    [[ "${model_success_count}" -ge "${model_count}" ]] && break

    retry_count=$(( $retry_count + 1 ))
    debug "Retry attempt ${retry_count} of ${max_retries}, only $model_success_count of $model_count models succeeded for $genome_file"
    sleep 60 # then do it again, hopefully when the state of the mainframe is more stable
  done
fi

# XXX: if failed max times, try to flockingly have headnode finish it

debug "retried taxon $retry_count out of a maximum $max_retries times."
debug "$model_success_count of $model_count models succeeded for $genome_file"
debug "done, started at $start_time."
