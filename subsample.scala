package parfam;

import java.util.StringTokenizer
import java.io.File
import java.io.PrintWriter // for writing files
import java.security.SecureRandom
import java.nio.ByteBuffer
import java.lang.Math
import scala.annotation
import scala.io.Source // for reading files
import scala.collection.mutable
import scala.actors.Actor
import scala.actors.Actor._

object HitterSignal extends Enumeration {
  val COMPLETE, SHUTDOWN = Value
}
object AdjusterSignal extends Enumeration {
  val COMPLETE, SHUTDOWN = Value
}
object ReaderSignal extends Enumeration {
  val COMPLETE, SHUTDOWN = Value
}

class Gene(_name: String, _count: Int, _lobound: Int, _hibound: Int) {
  val name = _name
  val count = _count // count of genes in this pfam
  def getCount: Int = { return count }

  var hitcount = 0
  def getHitcount: Int = { return hitcount }

  var lobound = _lobound
  var hibound = _hibound
  def lo: Int = { return lobound }
  def hi: Int = { return hibound }
  def decBounds: Unit = {
    lobound -= 1
    hibound -= 1
  }

  def hitPossible: Boolean = {
    return (hitcount < count)
  }
  def recordHit: Unit = {
    hitcount += 1
    hibound -= 1 // shrink number of genes that can be hit in this pfam
  }

  def unhitPossible: Boolean = {
    return (hitcount > 0)
  }
  def recordUnhit: Unit = {
    hitcount -= 1
    hibound -= 1 // shrink number of genes that can be unhit in this pfam
  }

  def incHitcount: Unit = {
    hitcount += 1 // don't adjust bounds yet, that's a later pass
  }

  def randAdjustHits(newlobound: Int): Int = {
    // may take more hits to adjust subsample to appropriate ratio
    // so, set bounds according to number of contained genes to hit yet
    // the larger the bounds here, the more hits may be taken
    lobound = newlobound
    hibound = lobound + count - hitcount
    return hibound
  }
  def randAdjustUnhits(newlobound: Int): Int = {
    // may undo some hits to adjust subsample to appropriate ratio
    // so, set bounds according to number of contained genes hit already
    // the larger the bounds here, the more hits may be undone
    lobound = newlobound
    hibound = lobound + hitcount
    return hibound
  }

  def report: String = {
    return ("%s\t%d" format (name, hitcount))
  }
}
class RandInt(_modulus: Int) {
  var modulus = _modulus
  val random = SecureRandom.getInstance("SHA1PRNG")

  def getModulus: Int = { return modulus }
  def decModulus: Unit = { modulus -= 1 }

  var cur_idx: Long = 0
  val randbytes = new Array[Byte](4) //32 bits
  def next: Int = {
    random.nextBytes(randbytes)
    val i: Int = Math.abs(ByteBuffer.wrap(randbytes).getInt)
    cur_idx = (cur_idx + i) % modulus
    return cur_idx.toInt
  }
}
class RandFloat(_modulus: Int) { // ensures even distribution of random values
  val modulus = _modulus
  val mf: Float = modulus.toFloat
  val random = new RandInt(modulus)

  def next: Float = {
    return (random.next.toFloat / mf)
  }
}
class RandHitter(_manager: RandManager, _hitteridx: Int, _subsample: Float) extends Actor {
  var genes: Array[Gene] = null
  val manager = _manager
  val hitteridx = _hitteridx
  val subsample = _subsample

  def hits(randf: RandFloat, gene: Gene): Unit = {
    for (i <- 0 to gene.getCount - 1) {
      if (randf.next < subsample) {
        gene.incHitcount // random number of hits _roughly_ to subsample probability
      }
    }
  }
  def setGenes(_genes: Array[Gene]): Unit = {
    genes = _genes
  }
  def act {
    val modulus = 1000000
    var randf = new RandFloat(modulus) // specifying subsample as % would only require modulus of 100 or so.  100000 is extra precision
    while (true) {
      receive {
        case HitterSignal.SHUTDOWN => {
          //println("hitter %d shutdown" format (hitteridx))
          manager ! (hitteridx, HitterSignal.SHUTDOWN) // inform manager of exit
          exit()
        }
        case HitterSignal.COMPLETE => {
          //println("hitter %d done" format (hitteridx))
          manager ! (hitteridx, HitterSignal.COMPLETE) // inform manager of work completion
          genes = null // need new genes to process
          randf = new RandFloat(modulus) // reset rand num generator for new work
        }
        case genesidx: Int => {
          //println("hitter %d geneidx %d" format (hitteridx, genesidx))
          hits(randf, genes(genesidx))
          // ask for more work, saying how many samples were taken)
          manager ! (hitteridx, genes(genesidx).getHitcount)
        }
        case oops => {
          throw new Exception("hitter %d got unexpected data:\n%s"
                              format(hitteridx, oops))
        }
      }
    }
  }
}
class GeneReader(_genefiles: List[File], _manager: RandManager, _genestates: Array[GeneState]) extends Actor {
  var genefiles = _genefiles
  val manager = _manager
  val genestates = _genestates

  def read(genestatesidx: Int) {
    val genefile = genefiles.head
    genefiles = genefiles.tail
    val (genecount, genes) = RandomGroup.geneary(genefile)

    genestates(genestatesidx).setGenes(genecount, genes, genefile)
  }
  def act {
    while(true) {
      receive {
        case (genestatesidx: Int) => {
          read(genestatesidx)
          manager ! (ReaderSignal.COMPLETE, genestatesidx)
        }
        case ReaderSignal.SHUTDOWN => {
          manager ! ReaderSignal.SHUTDOWN
          exit()
        }
        case oops => {
          throw new Exception("reader got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
  }
}
class RandAdjuster(_subsample_ratio: Float, _manager: RandManager, _genestates: Array[GeneState]) extends Actor {
  val subsample_ratio = _subsample_ratio
  val manager = _manager
  val genestates = _genestates

  def adjustGenes(genestatesidx: Int): Unit = {
    var (samples_available, samples_needed, genes, genecount, genefile) =
      genestates(genestatesidx).get

    if (samples_available < samples_needed) {
      //println("take more samples")
      // take more samples
      var gcount = 0
      for(i <- 0 to genes.length - 1) {
        gcount = genes(i).randAdjustHits(gcount)
        //println("gcount " + gcount)
      }

      val random = new RandInt(gcount)
      while (samples_available < samples_needed) {
        samples_available += 1
        //println("more, avail " + samples_available)
        val idx = RandomGroup.randGene(genes, random)
        val gene = genes(idx)
        if (gene.hitPossible) {
          //println("  hit %d" format idx)
          gene.recordHit
          RandomGroup.shrinkGenePool(genes, idx) // shrinks pool of genes to hit
        } else {
          // try next pfams genes until got a hit
          for(i <- 1 to genes.length - 1) {
            val nextgeneidx = (i + idx) % genes.length
            //println("  hit try " + nextgeneidx)
            val nextgene = genes(nextgeneidx)
            if(nextgene.hitPossible) {
              //println("  hit2 %d" format idx)
              nextgene.recordHit
              RandomGroup.shrinkGenePool(genes, nextgeneidx)
            }
          }
        }
      }
    } else if (samples_available > samples_needed) {
      //println("undo some samples")
      // undo some samples
      var gcount = 0
      for(i <- 0 to genes.length - 1) {
        gcount = genes(i).randAdjustUnhits(gcount)
        //println("gcount " + gcount)
      }

      val random = new RandInt(gcount)
      while (samples_available > samples_needed) {
        samples_available -= 1
        //println("undo, avail " + samples_available)
        val idx = RandomGroup.randGene(genes, random)
        val gene = genes(idx)
        if (gene.unhitPossible) {
          //println("unhit %d" format idx)
          gene.recordUnhit
          RandomGroup.shrinkGenePool(genes, idx) // shrinks pool of hits
        } else {
          // try next pfams genes until got a hit
          for(i <- 1 to genes.length - 1) {
            val nextgeneidx = (i + idx) % genes.length
            val nextgene = genes(nextgeneidx)
            //println("unhit try " + nextgeneidx)
            if(nextgene.unhitPossible) {
              //println("unhit2 %d" format nextgeneidx)
              nextgene.recordUnhit
              RandomGroup.shrinkGenePool(genes, nextgeneidx)
            }
          }
        }
      }
    } // else got the perfect number of samples.  done.

    //XXX: should now print out the results here
    val outfilename = "%s.sub%d" format
      (genefile.getName, (subsample_ratio * 100).toInt)

    //XXX should print samples actual here
    //System.err.println("""samples actual     : %12d
//output file name   : %s""" format
    //  (samples_available, outfilename))

    val outfile = new PrintWriter(outfilename)
    for(gene <- genes) {
      outfile.println(gene.report)
    }
    outfile.close
  }
  def act {
    while(true) {
      receive {
        case genestatesidx: Int => {
          //println("adjusting.... " + genestatesidx)
          adjustGenes(genestatesidx)
          //println("adjusted " + genestatesidx)
          manager ! (AdjusterSignal.COMPLETE, genestatesidx)
          //could send signal to a printer object
        }
        case AdjusterSignal.SHUTDOWN => {
          manager ! AdjusterSignal.SHUTDOWN
          exit()
        }
        case oops => {
          throw new Exception("adjuster got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
  }
}
class GeneState() {
  var samples_available = -1
  var samples_needed = -1
  var genes: Array[Gene] = null
  var genecount = -1
  var genefile: File = null

  def clear: Unit = {
    samples_available = -1
    samples_needed = -1
    genes = null
    genecount = -1
    genefile = null
  }
  def setGenes(_genecount: Int, _genes: Array[Gene], _genefile: File): Unit = {
    genecount = _genecount
    genes = _genes
    genefile = _genefile
  }
  def getGenes(): (Int, Array[Gene], File) = {
    return (genecount, genes, genefile)
  }
  def set(_samples_available: Int, _samples_needed: Int, _genes: Array[Gene], _genecount: Int, _genefile: File): Unit = {
    samples_available = _samples_available
    samples_needed = _samples_needed
    genes = _genes
    genecount = _genecount
    genefile = _genefile
    //println("set genefile to " + _genefile)
    //println("set genefile is " + genefile)
  }
  def get(): (Int, Int, Array[Gene], Int, File) = {
    //println("genestate available: " + samples_available)
    //println("genestate needed: " + samples_needed)
    //println("genestate genes: " + genes)
    //println("genestate genecount: " + genecount)
    //println("genestate file: " + genefile)
    return (samples_available, samples_needed, genes, genecount, genefile)
  }
}
class RandManager(_genefileslen: Int, _genefiles: List[File], _subsample: Float, _num_hitters: Int) extends Actor {
  //var genefiles = _genefiles
  val genefileslen = _genefileslen
  val subsample_ratio = _subsample
  val hitters = new Array[RandHitter](_num_hitters)
  var genefilesread = 0
  var genefilesout = 0

  val genestates = new Array[GeneState](5)
  var genestatesidx_in  = 0 
  //var genestatesidx     = 0
  var genestatesidx_out = 0

  var genestateidx_in_backlog = -1

  val adjuster = new RandAdjuster(subsample_ratio, this, genestates)
  val reader = new GeneReader(_genefiles, this, genestates)

  def readGeneState(): Unit = {
    if (genefilesread < genefileslen) {
      genefilesread += 1 // count number of read requests
      reader ! genestatesidx_in
      genestatesidx_in = (genestatesidx_in + 1) % genestates.length
    }
  }
  def adjusterCompleted(genestateidx: Int): Unit = {
    genefilesout += 1 // just record that the gene file completed
    //in this wa, readers don't overflow buffers before writers catch up
  }
  //XXX: don't specify genefile.  actor should have array of them
  def incomingGeneState(): (Int, Array[Gene], File) = {
    // if not null, sleep???.  no, recieve.  wait in receive for incoming genestatesidx
    //genestates(genestatesidx_in).clear // XXX: actor does this, sets some state
    //receive here to get next incoming state TBD
    //genestatesidx_in = (genestatesidx_in + 1) % genestates.length

    if (genestateidx_in_backlog >= 0) {
      val idx = genestateidx_in_backlog
      genestateidx_in_backlog = -1
      return genestates(idx).getGenes
    } else {
      receive {
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          return genestates(genestateidx).getGenes
        }
        case (AdjusterSignal.COMPLETE, genestateidx: Int) => {
          adjusterCompleted(genestateidx)
        }
        case oops => {
          throw new Exception("manager %d got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    return (0, null, null)
  }
  def outgoingGeneState(samples_available: Int, samples_needed: Int, genes: Array[Gene], genecount: Int, genefile: File): Unit = {
    //println("d genefile: " + genefile)
    genestates(genestatesidx_out).set(samples_available, samples_needed,
                                      genes, genecount, genefile)
    adjuster ! genestatesidx_out
    genestatesidx_out = (genestatesidx_out + 1) % genestates.length
  }
  
  var cur_gene: Int = java.lang.Integer.MAX_VALUE // must set to 0
  def runHitter(rhit: RandHitter, genes: Array[Gene]): Unit = {
    if (cur_gene < genes.length) {
      rhit ! cur_gene
    } else if (cur_gene == genes.length) {
      // give hitters the work completion message, but only once
      for (h <- hitters) {
        h ! HitterSignal.COMPLETE
      }
    }
    cur_gene += 1 // also prevents multiple shutdowns
  }
  def sampleGenes(): Unit = {
    //println("a genefile: " + genefile)
    val (genecount, genes, genefile) = incomingGeneState()
    //println("b genefile: " + genefile)

    System.err.println("input file name    : %s" format
      (genefile.getName))

    val samples_needed = (subsample_ratio * genecount).toInt
    System.err.println("""subsample ratio    : %.10f
gene count         : %12d
samples needed     : %12d""" format
      (subsample_ratio, genecount, samples_needed))

    cur_gene = 0 // for runHitter, to track position
    for (i <- 0 to hitters.length - 1) {
      hitters(i).setGenes(genes) // hit these genes
    }
    // give hitters a backlog of work, so always busy
    for (j <- 0 to 1) {
      for (i <- 0 to hitters.length - 1) {
        //println("work hitter %d" format i)
        runHitter(hitters(i), genes)
      }
    }

    var samples_available = 0
    var running_hitters = hitters.length
    while (running_hitters > 0) {
      receive { // assign work as hitters need more
        case (hitteridx: Int, HitterSignal.COMPLETE) => {
          //println("mgr: hitter done: %d" format hitteridx)
          running_hitters -= 1
        }
        case (hitteridx: Int, samples_taken: Int) => {
          //println("mgr: hitter samples: %d" format samples_taken)
          samples_available += samples_taken
          runHitter(hitters(hitteridx), genes)
        }
        case (AdjusterSignal.COMPLETE, genestateidx: Int) => {
          adjusterCompleted(genestateidx)
        }
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          genestateidx_in_backlog = genestateidx // process later
        }
        case oops => {
          throw new Exception("manager got unexpected data:\n%s"
                              format(oops))
        }
      }
    }

    System.err.println("samples unadjusted : %12d" format
      (samples_available))
    
    //XXX: cheats by calculating the actual.  better to have a printer actor to order writes to stderr.
    //now print out the results
    val outfilename = "%s.sub%d" format
      (genefile.getName, (subsample_ratio * 100).toInt)
    System.err.println("""samples actual     : %12d
output file name   : %s""" format
      (samples_needed, outfilename)) // should be samples_available, not samples_needed

    //println("c genefile: " + genefile)
    outgoingGeneState(samples_available, samples_needed, genes, genecount, genefile)
    //println("done w/ outgoing")

    //do not overflow buffers reading gene files, wait for prints/outgoing to catch up
    while (genefilesread > genefilesout + 1) {
      receive {
        case (AdjusterSignal.COMPLETE, genestateidx: Int) => {
          adjusterCompleted(genestateidx)
        }
        case (ReaderSignal.COMPLETE, genestateidx: Int) => {
          genestateidx_in_backlog = genestateidx // process later
        }
        case oops => {
          throw new Exception("manager %d got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    readGeneState // request another gene file read
  }
  def act {
    // create gene states buffer for incoming and outgoing gene states
    for (i <- 0 to genestates.length - 1) {
      genestates(i) = new GeneState
    }

    //spawn genereader, TBD
    reader.start
    readGeneState // read first file
    readGeneState // backlog of work

    //spawn hitters, will reuse
    for (i <- 0 to hitters.length - 1) {
      hitters(i) = new RandHitter(this, i, subsample_ratio)
      hitters(i).start
    }

    adjuster.start

    //process each file full of pfams each having gene counts
    for (i <- 0 to genefileslen - 1) {
//    while(genefiles.nonEmpty) {
//      val genefile = genefiles.head
      //println("genefile: " + genefile)
//      genefiles = genefiles.tail
//      sampleGenes(genefile)
      sampleGenes
    }

    //kill hitters, adjuster, and reader
    //println("shutting down")
    var live_hitters = hitters.length
    for (i <- 0 to live_hitters - 1) {
      hitters(i) ! HitterSignal.SHUTDOWN // send shutdown message
    }
    var live_adjuster = true
    adjuster ! AdjusterSignal.SHUTDOWN
    var live_reader = true
    reader ! ReaderSignal.SHUTDOWN
    while (live_adjuster && live_reader &&
           live_hitters > 0 && genefilesread > genefilesout) {
      receive { // assign work as hitters need more
        case (hitteridx: Int, HitterSignal.SHUTDOWN) => {
          //println("hitter " + hitteridx)
          //println("live " + live_hitters)
          live_hitters -= 1
        }
        case (AdjusterSignal.COMPLETE, genestateidx: Int) => {
          // gene file write finished
          adjusterCompleted(genestateidx)
        }
        case AdjusterSignal.SHUTDOWN => {
          live_adjuster = false
        }
        case ReaderSignal.SHUTDOWN => {
          live_reader = false
        }
        case oops => {
          throw new Exception("manager got unexpected data:\n%s"
                              format(oops))
        }
      }
    }
    //println("done")
    exit()
  }
}
object RandomGroup {
  def geneary(infile: File): (Int, Array[Gene]) = {
    val inlist = new mutable.ListBuffer[Gene]
    val inlines = Source.fromFile(infile).getLines
    var genecount = 0
    while(inlines.hasNext) {
      val tokens = new StringTokenizer(inlines.next)
      val name = tokens.nextToken
      val count = tokens.nextToken.toInt
      val lobound = genecount
      val hibound = genecount + count
      genecount = hibound
      inlist += new Gene(name, count, lobound, hibound)
    }
    return (genecount, inlist.toArray)
  }

  def shrinkGenePool(genes: Array[Gene], idx: Int): Unit = {
    for(i <- (idx + 1) to (genes.length - 1)) {
      genes(i).decBounds
    }
  }
  // binary search for geneid within gene bounds
  def findGene(genes: Array[Gene], geneid: Int, idx_lo: Int, idx_hi: Int): Int = {
    val idx = idx_lo + ((idx_hi - idx_lo) / 2)
    val gene = genes(idx)
    if (geneid < gene.lo) {
      return findGene(genes, geneid, idx_lo, idx - 1)
    } else if (geneid >= gene.hi) {
      return findGene(genes, geneid, idx + 1, idx_hi)
    } else {
      return idx
    }
  }

  // pick a random gene
  def randGene(genes: Array[Gene], random: RandInt): Int = {
    val geneid = random.next
    random.decModulus // accounts for one fewer genes to pick from next time
    return findGene(genes, geneid, 0, genes.length - 1)
  }

  val subsample_usage = """
Usage:
java -jar omics.jar randgrp worker_threads subsample_ratio infile1 infile2 ...

randgrp takes a random sample of the given subsample_ratio (0.0 to 1.0)
of the given input files.

Subsamples are taken WITHOUT replacement.
For subsamples WITH replacement, use the randgrpr command, not randgrp.

The number of compute-intensive threads to use for cryptographically secure
random number generation is given by worker_threads.
There is a truly random seed for each random number generator.

Input file format:
groupname membercount
groupname membercount
...

Input file example:
A 2
B 0
C 0
D 4

The input file example is equivalent to...
A A D D D D
...and a random subsample is taken of that, without replacement.
"""
  def usage(msg: String): Unit = {
    if (msg != "") {
      println(msg)
    }
    println(subsample_usage)
    sys.exit(1)
  }
  def runArgs(args: Array[String]) {
    if (args.length < 3) {
      usage("Too few args")
    }
    val worker_threads = args(0).toInt
    val subsample_ratio = args(1).toFloat

    if (subsample_ratio <= 0.0 || subsample_ratio > 1.0) {
      usage("Must specify subsample_ratio between 0.0 and 1.0")
    }

    val genefiles = new mutable.ListBuffer[File]
    var genefileslen = 0
    for(i <- 2 to args.length - 1) {
      val genefile = new File(args(i))
      if (!genefile.exists) {
        println("ERROR: File %s does not exist." format args(i))
        System.exit(1)
      }
      genefiles += genefile
      genefileslen += 1
    }

    System.err.println("worker threads     : %12d" format
      (worker_threads))

    val rm = new RandManager(genefileslen, genefiles.toList, subsample_ratio, worker_threads)
    rm.start
  }
}
