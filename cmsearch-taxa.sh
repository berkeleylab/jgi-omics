#!/bin/bash
#
# parallel cmsearch across all taxa, using xargs
#
# comments: aschaumberg@lbl.gov
# 2012

taxa_dir=${1:-/global/homes/s/schaumbe/scratch/schaumbe/img_core_v400}

# some notes on concurrency
# at 12, running out of file descriptors at site, failure message:
# fork: retry: Resource temporarily unavailable
#
# 12 would essentially fill genepool w/ work if idle, 20 notes per taxon
# 8 also runs out of fork resources
# 4 does, even.
max_concurrent_taxa=2

ls $taxa_dir/[0-9]*[0-9]/[0-9]*[0-9].fna | \
  sort | \
  xargs -L 1 -P $max_concurrent_taxa \
    --no-run-if-empty --verbose \
    cmsearch-hadoop-all.sh
