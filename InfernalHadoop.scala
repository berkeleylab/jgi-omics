import com.twitter.scalding._ // wraps Cascading, wraps Hadoop
import scala.collection.parallel._ // multiprocessing within a Hadoop mapper
import org.apache.commons.exec._ // exec'ing cmsearch etc
import java.io.File // for existence tests, touching
import java.io.PrintWriter // for writing files
import scala.io.Source // for reading files
//import scalax.io._ // for writing files, Resource

class InfernalJob(args : Args) extends Job(args) {
  private def cmsearch(model_file: File, genome_file: File, cmsearch_concurrency: Int): Int = {
    //set up output directory
    val cmsearch_dir = new File("%s/cmsearch" format genome_file.getParent)
    cmsearch_dir.mkdir

    val model_outdir = new File("%s/%s".format(cmsearch_dir.getPath,
                                               model_file.getName))
    model_outdir.mkdir

    // description of output files
    // *.cmsearch.out: stdout of cmsearch 1.1
    // *.tblout: tabular output of cmsearch 1.1
    // *.tabfile: tabular output derived from !.tblout, in the format of cmsearch 1.0,
    //            for use with legacy libraries that expect cmsearch 1.0 files
    // *.success: exists if cmsearch stuff completed without error, created here
    val cmsearch_tbloutfile = new File("%s/cmsearch.tblout"  format model_outdir.getPath)
    val cmsearch_outfile    = new File("%s/cmsearch.out"     format model_outdir.getPath)
    val cmsearch_tabfile    = new File("%s/cmsearch.tabfile" format model_outdir.getPath)
    val cmsearch_success    = new File("%s/cmsearch.success" format model_outdir.getPath)

    var exitcode = 0
    if (cmsearch_success.exists) {
      println("cmsearch model " + model_file.getName + " (skipping)")
      return exitcode
    }

    val commandline = new org.apache.commons.exec.CommandLine("cmsearch") // XXX: write tailrec func to build cmdline from list
    commandline.addArgument("--cpu")
    commandline.addArgument(cmsearch_concurrency.toString)
    commandline.addArgument("--cut_ga")
    commandline.addArgument("--tblout")
    commandline.addArgument(cmsearch_tbloutfile.getPath)
    commandline.addArgument("-o")
    commandline.addArgument(cmsearch_outfile.getPath)
    commandline.addArgument(model_file.getPath)
    commandline.addArgument(genome_file.getPath)

    val exec        = new org.apache.commons.exec.DefaultExecutor
    exitcode = exec.execute(commandline)
    println("cmsearch model " + model_file.getName + " exitcode: " + exitcode)



    // now convert cmsearch 1.1 tabular out to cmsearch 1.0 tabular out.
/* cmsearch 1.1 tabular format
#target name            accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#---------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
sludgePhrap_Contig4094  -         5S_rRNA              RF00001    cm        1      119      493      607      +    no    1 0.61   0.7   84.6   6.8e-17 !   -
sludgePhrap_Contig4094  -         5S_rRNA              RF00001    cm        1      119      493      607      +    no    1 0.61   0.7   84.6   6.8e-17 !   -
...
*/
/* cmsearch 1.1 tabular format w/ description having whitespace
#target name         accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
hgutS7_s7_161241     -         5S_rRNA              RF00001    cm        1      119      240      356      +    no    1 0.53   0.1   95.0   4.1e-20 !   Human Gut Community Subject 7 : hgutS7_s7_161241
hgutS7_s7_167005     -         5S_rRNA              RF00001    cm        1      119     2386     2501      +    no    1 0.66   5.3   93.0   1.3e-19 !   Human Gut Community Subject 7 : hgutS7_s7_167005
hgutS7_s7_170699     -         5S_rRNA              RF00001    cm        1      119      787      671      -    no    1 0.53   0.1   90.2     7e-19 !   Human Gut Community Subject 7 : hgutS7_s7_170699
*/
/* cmsearch 1.0 tabular format
# CM: purine.2-1
#                                                target coord   query coord                         
#                                      ----------------------  ------------                         
# model name  target name                   start        stop  start   stop    bit sc   E-value  GC%
# ----------  -----------------------  ----------  ----------  -----  -----  --------  --------  ---
  purine.2-1  sludgePhrap_Contig13331        1512        1410      1    103     24.22  2.00e-01   37
  purine.2-1  sludgePhrap_Contig13331        1512        1410      1    103     24.22  2.00e-01   37
...
*/
    val cmsearch_tbloutfile_iterator = Source.fromFile(cmsearch_tbloutfile).getLines
    val cmsearch_tabfile_handle = new PrintWriter(cmsearch_tabfile)
    cmsearch_tabfile_handle.println("# %32s  %32s  %12s  %12s  %12s  %12s  %8s  %8s  %3s" format (
                                    "model_name", "target_name", "target_start", "target_stop",
                                    "query_start", "query_stop", "bit_sc", "e_value", "gc%"))
    while (cmsearch_tbloutfile_iterator.hasNext) {
      val line = cmsearch_tbloutfile_iterator.next
      if (!line.startsWith("#")) {
        // parse cmsearch 1.1
        //val Array(target_name, target_accession, query_name, query_accession,
        //          mdl, mdl_from, mdl_to, seq_from, seq_to, strand, trunc, pass,
        //          gc_ratio, bias, bit_score, e_value, inc, target_description) =
        //          line.trim.split("""\s+""") // XXX: breaks if whitespace in target_description
        val fields      = line.trim.split("""\s+""")
        val target_name = fields(0)
        val query_name  = fields(2)
        val mdl_from    = fields(5)
        val mdl_to      = fields(6)
        val seq_from    = fields(7)
        val seq_to      = fields(8)
        val gc_ratio    = fields(12)
        val bit_score   = fields(14)
        val e_value     = fields(15)
        // write cmsearch 1.0
        cmsearch_tabfile_handle.println("  %32s  %32s  %12s  %12s  %12s  %12s  %8s  %8s  %3d" format
                                        (query_name, target_name,
                                         seq_from, seq_to, mdl_from, mdl_to,
                                         bit_score, e_value,
                                         (gc_ratio.toDouble * 100).toInt))
      }
    }
    cmsearch_tabfile_handle.close

    if (exitcode == 0) {
      cmsearch_success.createNewFile // finished, success!
    }
    return exitcode
  }

  def runModels(model_dir: File, genome_file: File, chunk_div: Int, chunk_id: Int, worker_concurrency: Int, cmsearch_concurrency: Int): String = {
    var model_files = model_dir.listFiles.sorted // sort by filename, in case filesystem doesn't (e.g. NFS)
    var model_file_count = -1
    try {
      model_file_count = model_files.length
    } catch {
      case e: NullPointerException => {
        println("ERROR, models directory inaccessible? " + model_dir)
        throw(e)
      }
    }

    val modelidx_first =
      ((chunk_id)   * model_file_count.toDouble / chunk_div).ceil
    val modelidx_last  =
      ((chunk_id+1) * model_file_count.toDouble / chunk_div).floor

    println("first model idx " + modelidx_first)
    println("last model idx " + modelidx_last)

    // filter out all model files outside first and last model indexes
    model_files = model_files.zip( // zip makes file,index tuples
                    0 to model_files.length - 1).filter( // filter out unwanted indexes
                    x => x._2 >= modelidx_first && x._2 <= modelidx_last).map( // remove indexes, leaving files
                    _._1)

    model_files foreach { f:File => println("todo model " + f.getPath) }

    //val model_files = model_paths.split("""\s+""").map(new File(_))
    // parallel collection of all infernal model files

    //XXX: scala 2.9 concurrency control
    collection.parallel.ForkJoinTasks.defaultForkJoinPool.setParallelism(worker_concurrency)

    val model_par = model_files.par

    //XXX: scala 2.10 concurrency control
    //model_par.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(worker_concurrency))

    val exitcodes = model_par.map(cmsearch(_, genome_file, cmsearch_concurrency))

    return exitcodes.mkString(",")
  }

  TextLine(args("input"))
    .map('line -> 'exitcode){ line : String => {
      val fields = line.split("""\s+""")
      val chunk_id = fields(0).toInt
      val chunk_div = fields(1).toInt
      val genome_file = new File(fields(2))
      val model_dir = new File(fields(3))
      //#val concurrency = fields(4).toInt
      val worker_concurrency = fields(4).toInt
      val cmsearch_concurrency = fields(5).toInt
      println("chunk id " + chunk_id)
      println("chunk div " + chunk_div)
      println("genome " + genome_file)
      println("models " + model_dir)
      println("num workers  " + worker_concurrency)
      println("num cmsearchers per worker " + cmsearch_concurrency)
      runModels(model_dir, genome_file, chunk_div, chunk_id, worker_concurrency, cmsearch_concurrency)
    }}
    .write(Tsv(args("output")))
}
