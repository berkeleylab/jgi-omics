#!/bin/bash

model_dir=$1
genome_fna=$2
parfam_jar=$3
genome_dir=$(dirname $genome_fna)
cmsearch_dir=$genome_dir/cmsearch

mkdir -p $cmsearch_dir

wd=$PWD
global_timing_dir=~/cmsearch-timings
mkdir $global_timing_dir
#(cd $global_timing_dir; ln -s $genome_dir)

for model in $model_dir/*; do
  model_base=$(basename $model)
  rundir=$cmsearch_dir/$model_base
  mkdir $rundir
  timefile=$rundir/cmsearch.time

  date +%Y%m%d%H%M%S >> $timefile
  hostname >> $timefile
  echo $PWD >> $timefile
  echo $cmd >> $timefile
  echo java -jar $parfam_jar --model $model --genome $genome_fna >> $timefile
  (time java -jar $parfam_jar --model $model --genome $genome_fna) >> $timefile 2>&1

  rsync -a $genome_dir $global_timing_dir
done
