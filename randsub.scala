package parfam {
  import java.io.File
  import java.util.Random
  import java.lang.Exception
  import scala.io.Source // for reading files
  import scala.collection.immutable.TreeMap
  import scala.collection.immutable.TreeSet

  object RandomSubsample {
    val randsub_stats_usage = """
Usage:
java -jar omics.jar randsub --file myfile --lines X

Takes a random subsample of the lines in myfile.
The subsample will have X lines.
TBD: allow a % to be expressed, rather than lines.

Memory footprint is proportional to the subsample size.
As X increases, so does memory footprint.
Memory footprint does not depend on myfile size.
"""
    def usage(msg: String): Unit = {
      if (msg != "") {
        println(msg)
      }
      println(randsub_stats_usage)
      sys.exit(1)
    }
    def runArgs(args: Array[String]): Unit = {
      if (args.length < 4) {
        usage("Too few args")
      }
      var badarg = false
      var infilename: String = null
      var lines = -1
      def nextOption(list: List[String]) : Unit = {
        list match {
          case Nil => {}
          case "--file" :: value :: tail => {
            infilename = value
            nextOption(tail)
          }
          case "--lines" :: value :: tail => {
            lines = value.toInt
            nextOption(tail)
          }
          case option :: tail => {
            println("Unknown option: " + option) 
            badarg = true
            nextOption(tail)
          }
        }
      }
      nextOption(args.toList)
      
      if (badarg) {
        usage("")
      }

      if (infilename == null) {
        usage("Please specify a --file")
      }
      val infile = new File(infilename)
      if (!infile.exists) {
        usage("Input --file doesn't exist")
      }

      if (lines < 1) {
        usage("Specify 1 or greater via --lines")
      }

      (new RandomSubsample(infile)).run(lines)
    }
  }
  class RandomSubHit(_randkey: Int) extends Ordered[RandomSubHit] {
    var hitlist = List[Int]()
    var hitlen = 0
    var randkey = _randkey

    def getKey(): Int = return randkey
    
    def compare(that: RandomSubHit) = this.randkey - that.getKey

    def hits(): List[Int] = {
      return hitlist
    }
    def empty(): Boolean = {
      return (hitlen == 0)
    }
    def add(id: Int): Unit = {
      hitlist ::= id
      hitlen += 1
    }
    def del(rand: Random): Option[Int] = {
      if (hitlen == 1) {
        val ret = hitlist(0)
        hitlist = List[Int]()
        hitlen = 0
        return Some(ret)
      } else if (hitlen < 1) {
        return None
      } else {
        val idx = rand.nextInt(hitlen) // random member to delete
        var newhitlist = List[Int]()
        var ret = -1
        var i = 0
        for(hit <- hitlist) {
          if (i == idx) {
            ret = hit
          } else {
            newhitlist ::= hit
          }
          i += 1
        }
        if (ret == -1) {
          throw new Exception("to remove element %d but only %s element(s) in list" format
            (idx, hitlen))
        }
        hitlen -= 1
        hitlist = newhitlist
        return Some(ret)
      }
    }
  }
  class RandomSubsample(_infile: File) {
    val infile = _infile
    val rand = new Random

    private def printableLines(subsamples: TreeMap[Int, RandomSubHit]): List[Int] = {
      var goodLines = TreeSet[Int]()
      for ((randkey, subhit) <- subsamples) {
        for (hit <- subhit.hits) {
          goodLines += hit
        }
      }
      return goodLines.toList
    }
    private def subsample(subsamplelen: Int): TreeMap[Int, RandomSubHit] = {
      var hits = new TreeMap[Int, RandomSubHit]
      val inlines = Source.fromFile(infile).getLines
      var curlen = 0
      var lineno = 0

      while (inlines.hasNext && curlen < subsamplelen) {
        val discard = inlines.next
        val key = rand.nextInt
        val value = hits.get(key).getOrElse(new RandomSubHit(key))
        value.add(lineno)
        hits += (key -> value) // clobbers element w/ same key
        curlen += 1
        lineno += 1
      }

      var (minkey, minsubhit) = hits.min

      while (inlines.hasNext) {
        val discard = inlines.next
        val key = rand.nextInt
        if (key > minkey) {
          var recalc_min = false

          minsubhit.del(rand) // remove a random hit having this key
          if (minsubhit.empty) {
            hits -= minkey // remove this key/line from tree
            recalc_min = true
          }

          //add this key/line to tree
          hits.get(key) match {
            case Some(subhit) => {
              subhit.add(lineno) // key already exists, just add it in
            }
            case None => {
              val value = new RandomSubHit(key)
              value.add(lineno)
              hits += (key -> value) // new key:value pair

              //calculate new minimum, which may be above key:value
              recalc_min = true
            }
          }

          if (recalc_min) {
            val min = hits.min // returns (minkey, minsubhit) tuple
            minkey = min._1
            minsubhit = min._2
          }
        } else if (key == minkey) {
          minsubhit.add(curlen) // same min key, just add this in
          curlen += 1
          // however, this makes the subsample too big!
          // in the end, take a random subsample of this subhit to
          // keep subsample proper size in a way that's unbiased
          // towards keeping hits near the start/end of the input
        } // else key less than minimum, so leave out of subsample

        lineno += 1
      }

      // next make sure subsample is of proper size
      while (curlen > subsamplelen) {
        minsubhit.del(rand)
        if (minsubhit.empty) {
          hits -= minkey

          val min = hits.min // returns (minkey, minsubhit) tuple
          minkey = min._1
          minsubhit = min._2
        }
      }

      return hits
    }
    def run(subsamplelen: Int): Unit = {
      var printablelines = printableLines( subsample(subsamplelen) )

      var lineno = 0
      val inlines = Source.fromFile(infile).getLines
      while (printablelines.nonEmpty && inlines.hasNext) {
        val line = inlines.next
        if (lineno == printablelines.head) {
          println(line)
          printablelines = printablelines.tail
        }
        lineno += 1
      }
    }
  }
}
