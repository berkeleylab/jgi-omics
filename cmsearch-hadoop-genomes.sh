#!/bin/bash

cmd=$0
all_models_count=2208
non_trna_models_count=2189

cmd_dir=$(pwd $cmd)
omics_jar=$cmd_dir/omics.jar

if [[ -d "$1" ]]; then
  img_genome_dir=$1
  models_dir=$2

  parallel_hadoops=4

  # XXX: don't head in production
  #ls $img_genome_dir/{651324055,2004178003}/[0-9]*[0-9].fna | \
  #ls $img_genome_dir/[0-9]*[0-9]/[0-9]*[0-9].fna | \
  ls $img_genome_dir/651324049/[0-9]*[0-9].fna | \
    sort -r | \
    xargs -L 1 -P $parallel_hadoops \
          -I GENOME_FNA \
          --verbose --no-run-if-empty \
          $cmd GENOME_FNA $models_dir
elif [[ -f "$1" ]]; then
  genome_fna=$1
  models_dir=$2
  
  genome_dir=$(dirname $genome_fna)
  
  cmsearch_dir_count=$(ls $genome_dir/cmsearch | wc -l)
  if [[ "$cmsearch_dir_count" -eq "$all_models_count" ||
        "$cmsearch_dir_count" -eq "$nontrna_models_count" ]]; then
    successes_count=$(ls $genome_dir/cmsearch/*/cmsearch.success | wc -l)
    if [[   "$cmsearch_dir_count" -eq "$all_models_count" && \
            "$successes_count"    -eq "$all_models_count" ]]; then
      exit 0 # all models done
    elif [[ "$cmsearch_dir_count" -eq "$non_trna_models_count" && \
            "$successes_count"    -eq "$non_trna_models_count" ]]; then
      exit 0 # all non-tRNA-rRNA models done
    fi
  fi
  
  set -x

  run_template=hadoop.$(date +%Y%m%dt%H%M%S).$$.XXXX
  runs_dir=~/runs
  run_dir=$(mktemp -d $runs_dir/$run_template)
  run=$(basename $run_dir)
  hadoop_infile=$run_dir/hadoop-input
  hadoop_outdir=$run_dir/hadoop-output

  mkdir -p $run_dir
  (cd $run_dir; ln -s $genome_dir) # link to the file that was processed

  rm $hadoop_infile
  for f in $models_dir/*; do
    echo "$genome_fna $f" >> $hadoop_infile
  done

  time hadoop jar $omics_jar cmsearch.hadoop $hadoop_infile $hadoop_outdir

  (cd $runs_dir; tar czf $run.tgz $run && rm -rf $run) &

  models_now_completed=$(ls $genome_dir/cmsearch/*/cmsearch.success | wc -l)
  models_existing=$(ls $models_dir/* | wc -l)
  if [[ "$models_now_completed" -eq "$models_existing" ]]; then
    taxon_oid=$(basename $genome_dir)
    img_dir=$(dirname $genome_dir)
    (cd $img_dir; tar czf $taxon_oid.hadoop.${models_now_completed}.tgz $taxon_oid)
  fi
  wait
  set -x
else
  echo "Usage: $cmd /path/to/img/dir/containing/genomes /path/to/dir/containing/models"
  exit 1
fi
